package uglycode.explosions.world
{
	import citrus.core.CitrusEngine;
	import citrus.core.StarlingState;
	import citrus.datastructures.DoublyLinkedListNode;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.sound.ISequencable;
	import uglycode.explosions.sound.Pattern;
	import uglycode.explosions.sound.Sequence;
	import uglycode.util.PoolObject;

	/**
	 * This is a manager class that has helper utilities for putting timed things into the world.
	 * The Sequencer itself is in explosions.sound package, not here.
	 */
	public class Spawner implements ISequencable
	{
		private var _sequence:Sequence;
		private var parentState:StarlingState;
		protected var _ce:Explosions;
		public var enemyPool:PoolObject;
		public var spawneeClass:Class;


		


		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Spawner(enemySequence:EnemySequence, pool:PoolObject, spClass:Class, state:StarlingState)
		{
			_ce = CitrusEngine.getInstance() as Explosions;
			
			enemySequence.parentObject = this;
			_ce.registry.sequencer.seq.push(enemySequence);
			this.enemyPool = pool;
			this.spawneeClass = spClass;
			this.parentState = state;
			
		}
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		public function get sequence():Sequence
		{
			return _sequence;
		}
		
		public function set sequence(value:Sequence):void
		{
			_sequence = value;
		}
		/**
		 * =================================================================== init ===========================================================================
		 */

		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		
		/**
		 * return true if the game object (gun,power,etc) can "fire" according to the game logic.
		 */
		public function canFire():Boolean {
			return true;
		}
		
		
		
		public function fire(type:uint = 1):void {
			switch(type) {
				case 1:
					break;
			}
			var misItem:DoublyLinkedListNode = enemyPool.create(this.getEnemyOptions());
			_ce.registry.stats.enemiesSpawned[String(enemyPool.poolType)]++;
			//add to the scene, yo
			parentState.add(misItem.data);
		}
		
		
		
		private function getEnemyOptions():Object
		{
			var tempOptions:Object = spawneeClass.getSpawnerOptions();
			tempOptions.parentState = parentState;
			tempOptions.parentSpawner = this;
			if (spawneeClass.viewType == 'image') {
				tempOptions.view = _ce.registry.assetManager.getImageFromAtlas(spawneeClass.viewGroup, spawneeClass.viewAtlas, spawneeClass.viewSprite);
			}
			else {
				tempOptions.view = _ce.registry.assetManager.getMovieFromAtlas(spawneeClass.viewGroup, spawneeClass.viewAtlas, spawneeClass.viewSprite);
			}
			return tempOptions;
			// TODO this we can use later for dropping loot if the whole sequence was destroyed
			
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
	}
}
package uglycode.explosions.world
{
	import citrus.objects.CitrusSprite;
	
	import flash.geom.Point;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	
	import uglycode.explosions.Explosions;
	import uglycode.util.AssetManager;
	import citrus.core.StarlingState;

	public class Backdrop
	{
		private var backdrop:Vector.<CitrusSprite>;
		private var backdrop2:Vector.<CitrusSprite>;
		private var backdropWidth:Vector.<Number>;
		private var assetManager:AssetManager;
		private var state:StarlingState;

		
		public function Backdrop(state:StarlingState, assetManager:AssetManager)
		{
			this.assetManager = assetManager;
			this.state = state;
		}

		
		
		public function createBackground():void
		{
			backdrop = new Vector.<CitrusSprite>();
			backdrop2 = new Vector.<CitrusSprite>();
			backdropWidth = new Vector.<Number>();
			
			var _img:Image = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-4');
			var scale:Number = uglycode.explosions.Explosions.STAGE_HEIGHT / _img.height;
			_img.scaleX = _img.scaleY = scale;
			var co:CitrusSprite = new CitrusSprite('backdrop41', {x:0, y:0, width:_img.width, height:_img.height, view: _img, group: 0});
			backdropWidth.push(co.view.width);
			co.view.blendMode = starling.display.BlendMode.NONE;
			state.add(co);
			backdrop.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-4');
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop42', {x:_img.width, y:0, width:_img.width, height:_img.height, view: _img, group: 0});
			co.view.blendMode = starling.display.BlendMode.NONE;
			state.add(co);
			backdrop2.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-3');
			scale = uglycode.explosions.Explosions.STAGE_HEIGHT / _img.height;
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop31', {x:0, y:0, width:_img.width, height:_img.height, view: _img});
			backdropWidth.push(co.view.width);
			co.view.smoothing = 'trilinear';
			//co.view.alpha = 0.999;
			co.view.blendMode = starling.display.BlendMode.NORMAL;
			state.add(co);
			backdrop.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-3');
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop32', {x:_img.width , y:0, width:_img.width, height:_img.height, view: _img});
			co.view.smoothing = 'trilinear';
			//co.view.alpha = 0.999;
			co.view.blendMode = starling.display.BlendMode.NORMAL;
			state.add(co);
			backdrop2.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-2');
			scale = uglycode.explosions.Explosions.STAGE_HEIGHT / _img.height;
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop21', {x:0, y:0, width:_img.width, height:_img.height, view: _img});
			backdropWidth.push(co.view.width);
			co.view.smoothing = 'trilinear';
			//co.view.alpha = 0.999;
			co.view.blendMode = starling.display.BlendMode.NORMAL;
			state.add(co);
			backdrop.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-2');
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop22', {x:_img.width, y:0, width:_img.width, height:_img.height, view: _img});
			co.view.smoothing = 'trilinear';
			//co.view.alpha = 0.999;
			co.view.blendMode = starling.display.BlendMode.NORMAL;
			state.add(co);
			backdrop2.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-1');
			scale = uglycode.explosions.Explosions.STAGE_HEIGHT / _img.height;
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop11', {x:0, y:0, width:_img.width, height:_img.height, view: _img});
			co.view.smoothing = 'trilinear';
			backdropWidth.push(co.view.width);
			state.add(co);
			backdrop.push(co);
			
			_img = this.assetManager.getImageFromAtlas('temp','backdrop','backdrop-1');
			_img.scaleX = _img.scaleY = scale;
			co = new CitrusSprite('backdrop12', {x:_img.width, y:0, width:_img.width, height:_img.height, view: _img});
			co.view.smoothing = 'trilinear';
			state.add(co);
			backdrop2.push(co);
			
		}
		
		
		
		public function updateBackground(timeDelta:Number):void
		{
			var readyBackdrops:int = backdrop.length;
			var idx:int = 0;
			var scaling:Array = [0.4,0.06,3,7];
			var dx:Number = 0;
			var p:Point;
			var width:Number;
			var i:CitrusSprite;
			for (idx = 0; idx < readyBackdrops; idx++) 
			{
				i = backdrop[idx];
				width = backdropWidth[idx];
				//todo use game speed (music tempo)
				dx = (timeDelta * scaling[idx] * 15);
				backdrop2[idx].x -= dx;
				i.x -= dx;
				
				if (i.x < -width) {
					i.x = backdrop2[idx].x + width;
				}
				if (backdrop2[idx].x < -width) {
					backdrop2[idx].x = i.x + width;
				}
			}
		}
		
		
		
		public function destroy():void {
			//mruuuuh
			for (var i:int = 0; i < backdrop.length; i++) 
			{
				state.remove(backdrop[i]);
				state.remove(backdrop2[i]);
			}
		}
	}
}
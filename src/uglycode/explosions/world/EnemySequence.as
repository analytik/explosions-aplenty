package uglycode.explosions.world
{
	//import com.citrusengine.core.CitrusEngine;
	
	//import uglycode.explosions.sound.ISequencable;
	import flash.system.System;
	import flash.utils.getQualifiedClassName;
	
	import uglycode.explosions.sound.Pattern;
	import uglycode.explosions.sound.Sequence;
	
	public class EnemySequence extends Sequence
	{
		//public var patterns:Vector.<Pattern> = new Vector.<Pattern>;
		//public var totalBeats:uint;
		//private var lastBeatPlayed:uint;
		//public var parentObject:ISequencable;
		//protected var totalPatterns:int;
		public var stopSpawningAfterQueue:Boolean = true;
		public var empty:Boolean;
		
		
		public function EnemySequence()
		{
			super();
		}

		
		
		
		override public function initRandomSequence():void {
			patterns.push(
				new Pattern([
					0,1,0,0,2, 0,0,0,0,1, 0,1,1,0,0, 0,0,0,3,3,
					1,0,1,0,0, 0,0,0,0,1, 0,1,0,0,0, 1,0,1,0,0,
					1,0,1,0,0, 0,0,0,1,1
				], '', 50)
			);
			recountBeats();
		}
		
		public function initSymmetricFifthSequence():void {
			patterns.push(
				new Pattern([
					0,1,0,0,0, 0,1,0,0,0, 0,1,0,0,0, 0,1,0,0,0,
					0,1,0,0,0, 0,1,0,0,0, 0,1,0,0,0, 0,1,0,0,0,
					0,1,0,0,0, 0,1,0,0,0
				], '', 50)
			);
			recountBeats();
		}
		
		public function initSymmetricEightSequence():void {
			patterns.push(
				new Pattern([
					0,1,0,0,0, 0,0,0,0,1, 0,0,0,0,0, 0,0,1,0,0,
					0,0,0,0,0, 1,0,0,0,0, 0,0,0,1,0, 0,0,0,0,0,
					0,1,0,0,0, 0,0,0,0,1
				], '', 50)
			);
			recountBeats();
		}
		
		public function initFleet10Sequence():void {
			patterns.push(
				new Pattern([
					0,0,0,0,0, 1,1,1,1,1, 1,1,1,1,1, 0,0,0,0,0,
					0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
					0,0,0,0,0, 0,0,0,0,0
				], '', 50)
			);
			recountBeats();
		}
		
		public function initDifficultSequence():void {
			patterns.push(
				new Pattern([
					1,1,0,0,2, 0,0,0,0,1, 0,1,1,1,1, 0,2,2,3,3,
					1,0,1,0,0, 0,0,0,0,1, 0,1,0,0,0, 1,0,1,0,0,
					1,0,1,0,0, 0,0,0,1,1
				], '', 50)
			);
			recountBeats();
		}

		public function initDebugSequence():void {
			patterns.push(
				new Pattern([
					0,0,0,1,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
					0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
					0,0,0,0,0, 0,0,0,0,0
				], '', 50)
			);
			recountBeats();
		}

		// instrument = enemy
		//override public function initSequence(sequence:Array, instrument:String, beats:uint):void { }		
			
		
		/**
		 * Decide whether to spawn or not
		 * Loop the sequence if stopSpawningAfterQueue === false
		 * Stop spawning if it is true
		 * @param beat indexed from zero, of course, and so is "pos"
		 */
		override public function play(beat:uint):Boolean {
			if (empty === false && stopSpawningAfterQueue === true && beat >= totalBeats) {
				empty = true;
				trace ('ran out of Enemy beats! ' + beat + '/' + totalBeats);
				return false;
			}
			else if (empty === true) {
				return false;
			}
			var pos:uint = beat % totalBeats;
			//var canPlay:Boolean = parentObject.canFire();
			if (lastBeatPlayed == beat) {
				return false;
			}
			
			var i:int = 0;
			while (lastBeatPlayed != beat && i < totalPatterns)
			{
				if (pos >= patterns[i].beats) {
					// maybe this isn't the fastest solution, but it ensures sequences can be of variable length
					pos -= patterns[i].beats;
					i++;
					continue;
				}
				if (patterns[i].pattern[pos] !== 0) {
					//trace ('pattern: ' + patterns[i].pattern + '; result: ' + patterns[i].pattern[pos] + '; current step: ' + pos + '/' + beat + '; object: ' + getQualifiedClassName((parentObject as Spawner).spawneeClass));
					parentObject.fire(patterns[i].pattern[pos]);
					/*if (patterns[i].instrument != '') {
						CitrusEngine.getInstance().sound.playSound(patterns[i].instrument, 1, 0);
					}*/
					lastBeatPlayed = beat;
					return true;
				}
				else {
					break;
				}
			}
			return false;
			
		}
	
	}
}
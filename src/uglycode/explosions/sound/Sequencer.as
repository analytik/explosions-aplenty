package uglycode.explosions.sound
{
	import flash.utils.getTimer;
	
	import uglycode.explosions.actors.items.Power;

	public class Sequencer
	{
		

		public var currentBeat:uint = 0;
		/**
		 * Add sequences here if you want them to be updated automatically.
		 */
		public var seq:Vector.<Sequence> = new Vector.<Sequence>;
		
		private var _bpm:uint;
		private var _timeForBeat:Number; 
		private var lastBeatMs:uint;
		private var ms:uint;
		private var diff:uint;
		private var sequencableItems:Array = [];
		
		

		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Sequencer(bpm:uint = 220)
		{
			// please note that it doesn't make sense to make it higher than FPS*60 - 3600bpm (i.e. we can't get accuracy more than 16.6-20ms)
			// also this means that any beat will be offset by 0~20ms (or more, if framerate drops).
			// Blame Flash and the lack of my time to implement through SampleDataEvent.SAMPLE_DATA.
			// http://help.adobe.com/en_US/as3/dev/WSE523B839-C626-4983-B9C0-07CF1A087ED7.html
			this.bpm = bpm;
			lastBeatMs = getTimer();
			//blah blah
			//tempo
			//beats in a sequence
			//dunno
			//current timestamp
			//current beat
		}
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		public function get bpm():uint
		{
			return _bpm;
		}
		
		public function set bpm(value:uint):void
		{
			_bpm = value;
			_timeForBeat = 60000 / _bpm; //in miliseconds
		}
		
		public function get timeForBeat():Number
		{
			return _timeForBeat;
		}
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
			
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function update(timeDelta:Number):void {
			//fuck trace(timeDelta);
			ms = getTimer();
			// figure out if the time difference made it to another beat
			diff = ms - lastBeatMs;
			if (diff >= timeForBeat) {
				//if we skip a beat due to a lag, we won't bother, as we just don't give a fuck
				currentBeat++;
				//only update the last timer if we actually made it to another beat, DUH
				lastBeatMs = ms;
				
				var currentSequence:Sequence;
				for each (currentSequence in this.seq)
				{
					currentSequence.play(currentBeat);
				}
				
				for (var i:int = 0; i < sequencableItems.length; i++)
				{
					this.sequencableItems[i].update();
				}
			}
		}
		
		
		
		/**
		 * 
		 * Use this in state.init so you don't skip a beat whili loading assets / setting up the scene / etc
		 */
		public function reset():void {
			ms = lastBeatMs = getTimer();
			currentBeat = 0;
		}

		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		public function addToSequencing(thing:ISequenceTimed):void
		{
			trace('now sequencing '+thing);
			this.sequencableItems.push(thing);
		}
		
		public function removeFromSequencing(thing:ISequenceTimed):void
		{
			for (var i:int = 0; i < sequencableItems.length; i++) 
			{
				if (sequencableItems[i] === thing) {
					trace('now NOT sequencing anymore '+thing);
					sequencableItems.splice(i, 1);
					break;
				}
			}
			
		}
	}
}
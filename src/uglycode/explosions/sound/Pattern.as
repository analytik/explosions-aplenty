package uglycode.explosions.sound
{
	public class Pattern
	{
		public var beats:int = 16;
		public var pattern:Array;
		public var instrument:String;
		//public var lastBeatPlayed:uint = 0;
		
		public static var pattern_all_on:Array = [1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1];
		public static var pattern_fourth_0:Array = [1,0,0,0, 1,0,0,0, 1,0,0,0, 1,0,0,0];
		// TODO change instrument into a more complex object with pitch, effects, etc 
		//TODO add constants with some default patterns
		
		public function Pattern(pattern:Array = null, instrument:String = 'gun', beats:int = 16)
		{
			if (pattern === null) {
				//set some shitty default
				pattern = pattern_all_on;
				//pattern = [0,1,0,0, 0,1,0,0, 0,1,0,0, 0,1,0,1];
			}
			else if (pattern.length > beats) {
				trace ('trimming the pattern');
				pattern = pattern.slice(0, beats);
			}
			else if (pattern.length < beats) {
				trace ('padding zeros to the end of the pattern');
				while (pattern.length < beats) {
					pattern.push(0);
				}
			}
			this.pattern = pattern;
			this.beats = beats;
			this.instrument = instrument;
		}
		
		
		// public function update (timeDelta) ???
	}
}
package uglycode.explosions.sound
{
	public interface ISequencable
	{
		/**
		 * return true if the game object (gun,power,etc) can "fire" according to the game logic.
		 */
		function canFire():Boolean;
		
		/**
		 * For spawnable things, control output directly from the sequence. I know, it's a mish-mash.
		 */
		function fire(param:uint = 0):void;
		
		function set sequence(value:Sequence):void;
		function get sequence():Sequence;
	}
}
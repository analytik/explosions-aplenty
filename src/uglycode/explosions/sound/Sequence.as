package uglycode.explosions.sound
{
	import citrus.core.CitrusEngine;

	public class Sequence
	{
		public var patterns:Vector.<Pattern> = new Vector.<Pattern>;
		//todo add setters, recalc this on seq change
		public var totalBeats:uint;
		public var parentObject:ISequencable;
		protected var lastBeatPlayed:uint = 4249486259; // Sorry for this abomination, but we need to count with beat 0 being played. Lame. My bad.
		protected var totalPatterns:int;
		
		public function Sequence()
		{
			//
		}
		
		public function initRandomSequence():void {
			patterns.push(
				new Pattern(),
				new Pattern([1,0,1,0, 0,0,0,0, 0,1,0,1, 0,0,0,0]),
				new Pattern([1,0,0,0, 1,0,0,0, 1,0,0,0, 1,0,0,0])
			);
			recountBeats();
		}

		public function initSequence(sequence:Array, instrument:String, beats:uint):void {
			patterns.push(
				new Pattern(sequence, instrument, beats)
			);
			recountBeats();
		}

		protected function recountBeats():void
		{
			totalBeats = 0;
			for each (var p:Pattern in patterns) 
			{
				totalBeats += p.beats;
			}
			totalPatterns = patterns.length;
			
		}
		
		// TODO refactor as well as with EnemySequence - this is the "wronger" version
		public function play(beat:uint):Boolean {
			var pos:uint = beat % totalBeats;
			var canPlay:Boolean = parentObject.canFire();
			if (!canPlay || lastBeatPlayed == beat) {
				return false;
			}

			for (var i:int = 0; i < totalPatterns; i++) 
			{
				if (pos >= patterns[i].beats) {
					// maybe this isn't the fastest solution, but it ensures sequences can be of variable length
					pos -= patterns[i].beats;
					continue;
				}
				else if (patterns[i].pattern[pos] === 1) {
					//trace ('pattern: ' + patterns[i].pattern + '; result: ' + patterns[i].pattern[pos] + '; current step: ' + pos + '; sound: ' + patterns[i].instrument);
					if (patterns[i].instrument != '') {
						CitrusEngine.getInstance().sound.playSound(patterns[i].instrument, 1, 0);
					}
					lastBeatPlayed = beat;
					return true;
				}
			}
			return false;
			
		}
	}
}
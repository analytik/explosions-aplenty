package uglycode.explosions {

	import citrus.core.StarlingCitrusEngine;
	
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	
	import uglycode.explosions.game.GameData;
	import uglycode.explosions.game.JetsGunsState;
	import uglycode.explosions.game.Registry;
	import uglycode.explosions.sound.Sequencer;
	import uglycode.util.AssetManager;

	[SWF(frameRate="30", height="480", width="800", pageTitle="Explosions!")]
	public class Explosions extends StarlingCitrusEngine {
		
		public static const STAGE_HEIGHT:int = 480;
		public var registry:Registry;
		
		public function Explosions() {
			Starling.handleLostContext = true;  // required on Android. Needs to be done before Starling init.
			this.stage.addEventListener(Event.RESIZE, onFirstResize);
			
			loadAssets();
		}

		
		
		protected function onFirstResize(event:Event):void
		{
			//this.setUpStarling(true, 1, new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight - 48)); //reserve mysterious 48px for android system toolbar
			this.setUpStarling(true, 1, null, new Rectangle(0, 0, 800, Explosions.STAGE_HEIGHT)); //800x480 minus a bit, because 800x450 is 16:9; 16:10 would require 800x500 or 768x480 or 691x432.. which is a weird number 
			this._starling.stage3D.addEventListener(Event.CONTEXT3D_CREATE, onContextCreated);
			var x:int = 77;
		}
		
		
		
		private function onContextCreated(event:Event):void
		{
			// set framerate to 30 in software mode
			if (this.starling.context.driverInfo.toLowerCase().indexOf("software") != -1) {
				this.starling.nativeStage.frameRate = 25;
				trace ('Fuck your shit computer and its mother!');
				/**TODO add Feathers message that your computer sucks */
			}
			gameData = new GameData;
			
			var firstState:JetsGunsState = new JetsGunsState();
			// TODO? remove registry from states, leave it only on the engine?
			this.registry = firstState.registry = new Registry();
			//we can init it here, but we need to reset it later in State.init so that we don't skip a few beats while loading the game
			registry.sequencer = new Sequencer();
			registry.assetManager = new AssetManager(null, null, this.stage);
			state = firstState;
		}
		
		
		
		private function loadAssets():void {
			//load a bunch of crap
			sound.addSound("bolt", "app:/assets/sounds_electro/bolt.mp3");
			sound.addSound("gun", "app:/assets/sounds_electro/gun.mp3");
			sound.addSound("player_hit_a_1", "app:/assets/sounds_electro/plrhit_a_1.mp3");
			sound.addSound("player_death_a_1", "app:/assets/sounds_electro/death_a_1.mp3");
			sound.addSound("expl_a_1", "app:/assets/sounds_electro/expl_a_1.mp3");
			sound.addSound("expl_a_2", "app:/assets/sounds_electro/expl_a_2.mp3");
			sound.addSound("expl_a_3", "app:/assets/sounds_electro/expl_a_3.mp3");
			sound.addSound("expl_b_1", "app:/assets/sounds_electro/expl_b_1.mp3");
			sound.addSound("expl_b_2", "app:/assets/sounds_electro/expl_b_2.mp3");
			sound.addSound("expl_b_3", "app:/assets/sounds_electro/expl_b_3.mp3");
			sound.addSound("missile_a", "app:/assets/sounds_electro/missile_a.mp3");
			sound.addSound("missile_b", "app:/assets/sounds_electro/missile_b.mp3");
			sound.addSound("green", "app:/assets/sounds_electro/green.mp3");
			sound.addSound("red", "app:/assets/sounds_electro/red.mp3");
		}
		

	}
}
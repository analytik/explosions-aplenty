package uglycode.explosions.game
{
	import flash.data.EncryptedLocalStore;
	import flash.utils.ByteArray;

	public class EncryptedStorage
	{
		private var encryptText:Object;
		public function EncryptedStorage()
		{
		}
		
		private function encrypt():void {
			var data:ByteArray = new ByteArray();
			data.writeUTFBytes(encryptText.text);//create a byte array out of the text we want to encrypt
			EncryptedLocalStore.setItem("myEncryptedData", data); //store the byte array into the encrypted local storage 
		}
		
		
		//read the data from the encrypted local store
		private function decrypt():void {
			//using the same key I used when I wrote the data, I reead the bytes back
			var bytes:ByteArray = EncryptedLocalStore.getItem("myEncryptedData");
			if(bytes)
				trace(bytes.readUTFBytes(bytes.bytesAvailable));
			else
				trace("There was no value to be read!");
		}
	}
}
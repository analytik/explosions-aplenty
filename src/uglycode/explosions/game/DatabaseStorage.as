package uglycode.explosions.game
{
	import flash.data.SQLStatement;
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.filesystem.File;
	
	public class DatabaseStorage
	{
		private var sqlConnectionSync:SQLConnection;
		private var subject:Object;
		private var message:Object;
		public function DatabaseStorage()
		{
		}
		
		//create the database and table
		private function createDb():void {
			//create the database file in the application storage folder
			var file:File = File.applicationStorageDirectory.resolvePath("my.db");
			sqlConnectionSync = new SQLConnection();//create a connection object
			sqlConnectionSync.open(file, SQLMode.CREATE);//create the database
			if(file.exists)
				return;
			//create a statement
			var createDb:SQLStatement = new SQLStatement();
			//create a table
			createDb.text = "CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT, subject VARCHAR(64), message VARCHAR(255))";
			createDb.sqlConnection = sqlConnectionSync; //set the connection that will be used
			createDb.execute();//execute the statement
		}
		
		
		//write the data
		private function writeData():void {
			var insert:SQLStatement = new SQLStatement(); //create the insert statement
			insert.sqlConnection = sqlConnectionSync; //set the connection
			insert.text = "INSERT INTO messages (subject, message) VALUES (?, ?)";
			insert.parameters[0] = subject.text;
			insert.parameters[1] = message.text;
			insert.execute();
			trace("The data was saved into the table!");
		}
		
		
		//read the data
		private function readData():void {
			var read:SQLStatement = new SQLStatement(); //create the read statemen
			read.sqlConnection = sqlConnectionSync; //set the connection
			read.text = "SELECT id, subject, message FROM messages ORDER BY id";
			read.execute();
			var result:SQLResult = read.getResult(); //retrieve the result of the query
			//myDatagrid.dataProvider = result.data; //display the array of objects into the data grid
		}
	}
}
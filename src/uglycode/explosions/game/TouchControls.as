package uglycode.explosions.game
{
	import flash.geom.Rectangle;
	
	import citrus.input.controllers.AVirtualJoystick;
	
	import nape.geom.AABB;
	
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import uglycode.explosions.actors.Ship;
	
	public class TouchControls extends AVirtualJoystick
	{
		public var ship:Ship;
		private var touch:Touch;
		private var touchActive:Boolean;
		private var targetX:int;
		private var targetY:int;
		private var radius:uint = 50;
		/**
		 * touch tolerance [px] - this many pixels beyond the ship control box, we'll still detect touch and process it
		 */
		private var tolerance:uint = 20;
		private var deadzone:Number = 0.45;
		private var rect:Rectangle;
		
		
		public function TouchControls(name:String, params:Object=null)
		{
			super(name, params);
		}
		
		override protected function initGraphics():void {}
		
		override protected function initActionRanges():void
		{
			//register default actions to value intervals
			_xAxisActions = new Vector.<Object>();
			_yAxisActions = new Vector.<Object>();
			
			addAxisAction("x", "left", -1, -deadzone);
			addAxisAction("x", "right", deadzone, 1);
			addAxisAction("y", "up", -1, -deadzone);
			addAxisAction("y", "down", deadzone, 1);
			/*
			// we could add slow moving by handling these actions, but we can as well do it by setting deadzone radius to 25px (0.5 on each axis out of 50px) 
			addAxisAction("x", "slowleft", -deadzone, -0.2);
			addAxisAction("x", "slowright", 0.2, deadzone);
			addAxisAction("y", "slowup", -deadzone, -0.2);
			addAxisAction("y", "slowdown", 0.2, deadzone);
			*/
			
			//addAxisAction("y", "duck", 0.8, 1);
			//addAxisAction("y", "jump", -1, -0.8);
			
		}
		
		
		
		override public function update():void
		{
			// todo maybe show touch graphics, aiming at enemy or sth?	
			if (touch || touchActive) {
				// Do not detect anything beyond the ship's control rectangle!
				this.rect = ship.getControlRectangle(); 
				rect.x -= tolerance; rect.y -= tolerance; rect.width += tolerance*2; rect.height += tolerance*2;
				if (!rect.contains(targetX, targetY)) {
					_xAxis = _yAxis = 0;
				}
				else {
					_xAxis = ((ship.x - targetX) > 0 ? -1 : 1) * (Math.abs(ship.x - targetX) >= radius ? 1 : Math.abs(ship.x - targetX)/radius);
					_yAxis = ((ship.y - targetY) > 0 ? -1 : 1) * (Math.abs(ship.y - targetY) >= radius ? 1 : Math.abs(ship.y - targetY)/radius);
					//trace ('going to ' + targetX + ' / ' + targetY + ' (ratio '+_xAxis + '/' + _yAxis + ')');
				}
				
				if ((_xAxis >= -0.01 && _xAxis <= 0.01) || (_yAxis >= -0.01 && _yAxis <= 0.01)) {
					//threshold of Axis values where no actions will be fired // actions will turned off.
					triggerAllOFF();
					return;
				}
				else {
					var a:Object; //action 
					var ratio:Number;
					var val:Number;
					
					if (_xAxisActions.length > 0) {
						for each (a in _xAxisActions) {
							ratio = 1 / (a.end - a.start);
							val = _xAxis <0 ? 1 - Math.abs((_xAxis - a.start)*ratio) : Math.abs((_xAxis - a.start) * ratio);
							if ((_xAxis >= a.start) && (_xAxis <= a.end)) {
								triggerVALUECHANGE(a.name, val);
							}
							else {
								triggerOFF(a.name, 0);
							}
						}
					}
					
					if (_yAxisActions.length > 0) {
						for each (a in _yAxisActions)
						{
							ratio = 1 / (a.start - a.end);
							val = _yAxis <0 ? Math.abs((_yAxis - a.end)*ratio) : 1 - Math.abs((_yAxis - a.end) * ratio);
							if ((_yAxis >= a.start) && (_yAxis <= a.end)) {
								triggerVALUECHANGE(a.name, val);
							}
							else {
								triggerOFF(a.name, 0);
							}
						}
					}
					
				}
			}
			// else it's just mouse moving or whatnot
			//but we still need to STOP the movement:
			if (!touch && !touchActive) {
				triggerAllOFF();
			}
			//clean up after processing one last frame after TouchPhase.ENDED
			if (this.touchActive === false && touch) {
				touch = null;
			}
		}
		
		
		
		public function registerShipControls(ship:Ship):void
		{
			this.ship = ship;
			ship.parentState.stage.addEventListener(TouchEvent.TOUCH, handleTouch);
		}
		
		
		
		private function handleTouch(e:TouchEvent):void {
			this.touch = e.getTouch(ship.parentState, TouchPhase.MOVED);
			if (!touch) {
				//trace ('not moved');
				this.touch = e.getTouch(ship.parentState, TouchPhase.BEGAN);
				if (touch) {
					//trace ('starting touch');
					this.touchActive = true;
				}
			}
			if (!touch) {
				this.touch = e.getTouch(ship.parentState, TouchPhase.ENDED);
				if (touch) {
					this.touchActive = false;
					//trace ('finishing up touch');
				}
			}
			if (touch) {
				targetX = touch.globalX;
				targetY = touch.globalY;
			}
		}
	}







	
}
package uglycode.explosions.game
{
	import flash.utils.Dictionary;

	/**
	 * A class keeping track of per-player statistics, useful and other.
	 * LTTODO: Online stuff, achievements
	 */
	public class Stats
	{
		public var timePlayed:Dictionary;
		public var enemiesKilled:Dictionary;
		public var weapons:Dictionary;
		public var enemyWeapons:Dictionary;
		public var enemiesSpawned:Dictionary;
		public var useless:Dictionary;
		
		private var _ePenisLength:uint; // presented as "score" to players
		public var ePenisTotal:uint;
		
		
		public function Stats()
		{
			//TODO load shit from data storage
			timePlayed = new Dictionary();
			timePlayed['onCasual'] = 0;
			timePlayed['onEasy'] = 0;
			timePlayed['onMedium'] = 0;
			timePlayed['onHard'] = 0;
			timePlayed['onInsane'] = 0;
			
			enemiesKilled = new Dictionary();
			enemiesKilled['Ufo1'] = 0;
			enemiesKilled['Ufo2'] = 0;
			enemiesKilled['_small'] = 0;
			enemiesKilled['_large'] = 0;
			enemiesKilled['_bosses'] = 0;
			
			enemiesSpawned = new Dictionary();
			enemiesSpawned['Ufo1'] = 0;
			enemiesSpawned['Ufo2'] = 0;
			enemiesSpawned['_small'] = 0;
			enemiesSpawned['_large'] = 0;
			enemiesSpawned['_bosses'] = 0;
			
			weapons = new Dictionary();
			weapons['_shotsFired'] = 0;
			weapons['MachineGunShotFired'] = 0;
			weapons['MachineGunShotHit'] = 0;
			weapons['MachineGunShotDamage'] = 0;
			
			enemyWeapons = new Dictionary();
			enemyWeapons['_enemyShotsFired'] = 0;
			enemyWeapons['EvilPlasmaShotFired'] = 0;
			enemyWeapons['EvilPlasmaShotHit'] = 0;
			enemyWeapons['EvilPlasmaShotDamage'] = 0;
		}

		public function get ePenisLength():uint
		{
			return _ePenisLength;
		}

		public function set ePenisLength(value:uint):void
		{
			this.ePenisTotal += value - ePenisLength;
			_ePenisLength = value;
		}

	}
}
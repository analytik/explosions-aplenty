package uglycode.explosions.game
{
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import feathers.controls.Button;
	import feathers.controls.Screen;
	import feathers.core.FeathersControl;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	import uglycode.explosions.actors.items.Power;
	import uglycode.explosions.ui.ButtonSlot;
	import uglycode.explosions.ui.IPowerButtonable;
	import uglycode.explosions.actors.Ship;

	/**
	 * This class should manage all "special powers" and in the future, I guess also "summon" buttons
	 * 
	 * It connects actors with UI.
	 */
	public class PowerManager implements IPowerButtonable
	{
		public static const SLOT_WEAPONS:String = 'weapons';
		
		private var names:Vector.<String> = new Vector.<String>;
		private var powers:Vector.<Object> = new Vector.<Object>;
		//private var buttons:Vector.<FeathersControl> = new Vector.<FeathersControl>;
		// temp queue to handle my retarded chain of events that causes that we don't know if we have UI yet or naht
		private var powersToAdd:Vector.<Object> = new Vector.<Object>;

		private var _buttonSlots:Dictionary = new Dictionary(true); //ButtonSlot

		
		//actually screw this, they should be managed from Power itself
		//private var executors:Vector.<Object>;
		
		public var registry:Registry;
		public var parentShip:Ship;
		
		//todo rewrite to just any DisplayObject?
		private var _uiObject:Screen;
		
		
		
		/**
		 * we need... do we need registry at all? Well, we need the UI object, so we can create buttons and bind them.
		 */
		public function PowerManager(reg:Registry) //, uiObject:Screen)
		{
			this.registry = reg;
		}
		
		
		
		public function addPower(pow:Power):void {
			pow.parentShip = parentShip;
			pow.registry = registry;
			this.names.push(getQualifiedClassName(pow));
			this.registry.sequencer.addToSequencing(pow);
			this.powers.push(pow);
			if (uiObject === null) {
				this.powersToAdd.push(pow);
			}
			else {
				//this.resetPowerButtons();
				this.addButtonFromPower(pow);
			}
			//this.buttons.push(uiObject.getButtonForSlot(pow.slot);
		}
		
		
		
		private function addButtonFromPower(pow:Power):void
		{
			addPowerButton(
				pow.name,
				pow.slot,
				registry.assetManager.getTextureFromAtlas(pow.viewGroup, pow.viewAtlas, pow.viewSprite),
				pow.fire,
				pow.onChange
			);
		}
		
		
		
		public function addPowerButton(ident:String, slot:String, texture:Texture, pushedHandler:Function, updateSignal:Signal):FeathersControl {
			trace('calling pwrMgr.addPowerButton');
			if (!this._buttonSlots.hasOwnProperty(slot)) {
				this._buttonSlots[slot] = new ButtonSlot(120, 480-46-16, slot);
			}

			//create a button and save it to ButtonSlot
			var butt:Button = new Button();
			
			//give it a texture
			butt.selectedUpSkin = butt.defaultSelectedSkin = butt.defaultSkin = new Image(texture);
			butt.nameList.add( 'powerButton' );
			
			//add it to slot - this will call invalidate()
			uiObject.addChild(butt);
			this._buttonSlots[slot].addButton(ident, butt);
			
			// signal/event witchcraft - first, register Power method that will handle the press of the button
			butt.addEventListener(Event.TRIGGERED, pushedHandler);
			
			//second, fuck this because it doesn't make sense
			//(uiObject as JngScreen).onSpecialAbility = new Signal(Class);
			
			//third, register local method to update UI when Power says so
			updateSignal.add(updateShipUI);
			
			return butt;
		}
		
		
		public function removePowerButton(ident:String, slot:String):void {
			if (!this._buttonSlots.hasOwnProperty(slot)) {
				return;
			}
			var butt:FeathersControl = this._buttonSlots[slot].getButton(ident);
			if (butt) {
				this._buttonSlots[slot].removeButton(ident);
				butt.removeEventListeners(Event.TRIGGERED);
				uiObject.removeChild(butt);
			}
			this._buttonSlots[slot].buttons[ident] = null;
		}
		

		
		
		public function get uiObject():Screen
		{
			return _uiObject;
		}

		
		
		public function set uiObject(value:Screen):void
		{
			/*if (!(value is IPowerButtonable)) {
				throw new Error('Sorry, but the UI thingy needs to implement IPowerButtonable!');
			}*/
			_uiObject = value;
			this.resetPowerButtons();
		}
		
		
		
		private function readdRemainingPowers():void
		{
			for each (var p:Power in powersToAdd)
			{
				this.addPower(p);
			}
			powersToAdd.length = 0;
		}
		
		
		
		public function resetPowerButtons():void
		{
			// trim all buttons and re-add them
			this.removePowerButtons();
			//this weird construct means "clone powers, umkay?"
			for (var i:int = 0; i < this.powers.length; i++)
			{
				this.powersToAdd.push(this.powers[i]);
			}
			this.powers.length = 0;
			this.names.length = 0;
			this.readdRemainingPowers();
		}
		
		
		
		public function removePowerButtons():void {
			for (var i:int = 0; i < powers.length; i++)
			{
				this.removePowerButton(powers[i].name, powers[i].slot);
				this.registry.sequencer.removeFromSequencing(powers[i]);
			}
			//this.buttons.length = 0;
			this.powersToAdd.length = 0;
		}
		
		public function removeAllPowers():void
		{
			this.powers.length = 0;
			this.powersToAdd.length = 0;
			this.names.length = 0;
			for each (var slot:ButtonSlot in _buttonSlots) 
			{
				for (var i:int = 0; i < slot.buttons.length; i++)
				{
					trace ('removing button from slot ' + slot.name + ', button #' + i);
					_uiObject.removeChild(slot.buttons[i]);
					slot.buttons[i] = null;
				}
			}
			
			//this.buttons.length = 0;
		}
		
		
		
		
		
		private function updateShipUI(slotName:String, name:String, cooldown:int):void
		{
			_buttonSlots[slotName].buttons[name].label = cooldown > 0 ? cooldown : '';
			_buttonSlots[slotName].buttons[name].alpha = cooldown > 0 ? 0.45 : 1;
		}

		
		
		
		
	}
}
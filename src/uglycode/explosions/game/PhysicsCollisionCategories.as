package uglycode.explosions.game
{
	public class PhysicsCollisionCategories
	{
		public static const SHIP:int = 1;
		public static const SHOT:int = 2;
		public static const MISSILE:int = 4;
		public static const ENEMYSHOT:int = 8;
		public static const ENEMYMISSILE:int = 16;
		public static const ENEMY:int = 32;
		public static const GROUNDENEMY:int = 64;
		//public static const MEH01:int = 128;
		public static const BOSS:int = 256;
		//public static const MEH0:int = 512;
		//public static const MEH1:int = 1024;
		//public static const MEH2:int = 2048;
		//public static const MEH3:int = 4096;
		public static const GROUND:int = 8192;
		public static const SCENERY:int = 16384;
		
		
	}
}
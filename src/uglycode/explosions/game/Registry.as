package uglycode.explosions.game
{
	import citrus.core.CitrusObject;
	import citrus.core.StarlingState;
	import citrus.datastructures.DoublyLinkedListNode;
	import citrus.view.CitrusView;
	import citrus.view.starlingview.StarlingArt;
	
	import flash.system.System;
	
	import starling.display.Image;
	
	import uglycode.explosions.actors.Ufo1;
	import uglycode.explosions.actors.Ufo2;
	import uglycode.explosions.actors.items.EvilPlasmaShot;
	import uglycode.explosions.actors.items.EvilRedShot;
	//import uglycode.explosions.actors.items.EvilGreenShot;
	import uglycode.explosions.actors.items.FireballGunShot;
	import uglycode.explosions.actors.items.MachineGunShot;
	import uglycode.explosions.actors.items.Missile;
	import uglycode.explosions.sound.Sequencer;
	import uglycode.util.AssetManager;
	import uglycode.util.PoolObject;

	
	/**
	 * Hello. I'm a bastard son of Registry and Dependency Injection. 
	 * And Pool Factory.
	 * Goan fuck yourself. 
	 */
	public final class Registry
	{
		//constants for pool size initiation, depending on level size / environment / etc
		public const SMALL:int = 1;
		public const MEDIUM:int = 2;
		public const LARGE:int = 3;
		public const HUGE:int = 4;
		public const LEVEL_01:int = 1001;
		
		// player shit
		public var missilePool:PoolObject;
		//public var missileImagePool:PoolObject;
		public var machineGunShotPool:PoolObject;
		public var fireballGunShotPool:PoolObject;
		
		// enemy shit
		public var ufo1Pool:PoolObject;
		public var ufo2Pool:PoolObject;
		public var evilPlasmaShotPool:PoolObject;
		public var evilRedShotPool:PoolObject;
		public var evilGreenShotPool:PoolObject;
		private var allPools:Array = [];
		private var enemyPools:Array = [];
		private var shotPools:Array = [];

		// game objects / managers 
		public var assetManager:AssetManager;
		public var sequencer:Sequencer;
		public var stats:Stats;
		
		//private var spritePools:Array = [];
		
		private var cnt:uint;
		
		
		
		public function Registry()
		{
			//we cannot instantiate pool with Nape objects, because, well, we don't have Nape at startup, morons
			//missilePool = new PoolObject(Missile, 400, 20, true);
			// TODO move to [human]Player manager class once we have it
			this.stats = new Stats();
		}
		
		
		
		public function init(size:int):void {
			switch (size) {
				case SMALL:
					missilePool = new PoolObject(Missile, 10, 10, true);
					//missileImagePool = new PoolObject(CitrusObject, 30, 10, false);
					machineGunShotPool = new PoolObject(MachineGunShot, 40, 10, true);
					fireballGunShotPool = new PoolObject(FireballGunShot, 10, 10, true);
					ufo1Pool = new PoolObject(Ufo1, 10, 10, true);
					ufo2Pool = new PoolObject(Ufo2, 10, 10, true);
					evilPlasmaShotPool = new PoolObject(EvilPlasmaShot, 20, 10, true);
					evilRedShotPool = new PoolObject(EvilRedShot, 20, 10, true);
					break;
				case MEDIUM:
					missilePool = new PoolObject(Missile, 25, 15, true);
					//missileImagePool = new PoolObject(CitrusObject, 50, 15, false);
					machineGunShotPool = new PoolObject(MachineGunShot, 60, 10, true);
					fireballGunShotPool = new PoolObject(FireballGunShot, 10, 10, true);
					ufo1Pool = new PoolObject(Ufo1, 20, 10, true);
					ufo2Pool = new PoolObject(Ufo2, 20, 10, true);
					evilPlasmaShotPool = new PoolObject(EvilPlasmaShot, 30, 10, true);
					evilRedShotPool = new PoolObject(EvilRedShot, 30, 10, true);
					break;
				case LARGE:
					missilePool = new PoolObject(Missile, 40, 20, true);
					//missileImagePool = new PoolObject(CitrusObject, 100, 25, false);
					machineGunShotPool = new PoolObject(MachineGunShot, 120, 20, true);
					fireballGunShotPool = new PoolObject(FireballGunShot, 20, 10, true);
					ufo1Pool = new PoolObject(Ufo1, 30, 10, true);
					ufo2Pool = new PoolObject(Ufo2, 30, 10, true);
					evilPlasmaShotPool = new PoolObject(EvilPlasmaShot, 40, 10, true);
					evilRedShotPool = new PoolObject(EvilRedShot, 40, 10, true);
					break;
				case HUGE:
					missilePool = new PoolObject(Missile, 80, 40, true);
					//missileImagePool = new PoolObject(CitrusObject, 300, 40, true);
					machineGunShotPool = new PoolObject(MachineGunShot, 200, 50, true);
					fireballGunShotPool = new PoolObject(EvilRedShot, 20, 10, true);
					ufo1Pool = new PoolObject(Ufo1, 40, 10, true);
					ufo2Pool = new PoolObject(Ufo2, 40, 10, true);
					evilPlasmaShotPool = new PoolObject(EvilPlasmaShot, 50, 10, true);
					evilRedShotPool = new PoolObject(EvilRedShot, 50, 10, true);
					break;
				case LEVEL_01:
					// etc
					break;
			}
			allPools.push(missilePool);
			shotPools.push(missilePool);
			allPools.push(machineGunShotPool);
			shotPools.push(machineGunShotPool);
			allPools.push(fireballGunShotPool);
			shotPools.push(fireballGunShotPool);
			
			allPools.push(ufo1Pool);
			enemyPools.push(ufo1Pool);
			allPools.push(ufo2Pool);
			enemyPools.push(ufo2Pool);
			allPools.push(evilPlasmaShotPool);
			shotPools.push(evilPlasmaShotPool);
			//allPools.push(missileImagePool);
		}

		
		
		public function updatePools(timeDelta:Number, view:CitrusView):void {
			for (var i:int = 0; i < allPools.length; i++) 
			{
				if (allPools[i].isCitrusObjectPool) {
					//allPools[i].updatePhysics(timeDelta);
					allPools[i].cleanupPhysics();
				}
				else {
					trace ('fuck if I know what to do with pooled art! DO SOMETHING!');
					//allPools[i].updateArt(view);
				}
			}
		}
		
		
		
		/*public function updateSprites(stateView:CitrusView):void {
			for (var i:int = 0; i < spritePools.length; i++) 
			{
				spritePools[i].updateArt(stateView);
			}
		}*/
		
		
		
		/*public function cleanupLeftovers():void {
			if (cnt++ % 120 !== 0) return; 
			trace('starting cleanup');
			for (var i:int = 0; i < physicsPools.length; i++) {
				physicsPools[i].cleanupLeftovers();
			}
			for (i = 0; i < spritePools.length; i++) {
				spritePools[i].cleanupLeftovers();
			}
		}*/
		
		
		/**
		 * We need the state to remove graphics
		 */
		public function destroyAll(state:StarlingState):void {
			disposeAll(state);
			clearAll(state);
			/*
			while (missileImagePool.head) {
				state.remove(missileImagePool.disposeNode(missileImagePool.head).data);
			}
			*/
			//missilePool = null; //this is dumbbbbb
			flash.system.System.pauseForGCIfCollectionImminent(0.01);
		}
		
		private function clearAll(state:StarlingState):void
		{
			for (var i:int = 0; i < allPools.length; i++) 
			{
				allPools[i].clear();
			}
		}
		
		
		
		public function disposeAll(state:StarlingState):void
		{
			for (var i:int = 0; i < allPools.length; i++) 
			{
				while (allPools[i].head) {
					state.remove(allPools[i].disposeNode(allPools[i].head).data);
				}
				//allPools[i].disposeAll();
			}
		}		
		
		
		
		public function enemyPoolsAreEmpty():Boolean
		{
			return (getEnemyCount() == 0);
		}
		
		
		
		public function getEnemyCount():uint
		{
			var ret:uint;
			for (var i:int = 0; i < enemyPools.length; i++)
			{
				ret += enemyPools[i].length;
			}
			return ret;
		}
		
		
		
		public function getShotCount():uint
		{
			var ret:uint;
			for (var i:int = 0; i < shotPools.length; i++) 
			{
				ret += shotPools[i].length;
			}
			return ret;
		}
	}
}
package uglycode.explosions.game
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import citrus.core.CitrusEngine;
	import citrus.core.CitrusObject;
	import citrus.core.StarlingCitrusEngine;
	import citrus.core.StarlingState;
	import citrus.datastructures.DoublyLinkedListNode;
	import citrus.input.controllers.Keyboard;
	import citrus.math.MathVector;
	import citrus.objects.CitrusSprite;
	import citrus.physics.nape.Nape;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.actors.Ufo1;
	import uglycode.explosions.actors.items.FireballGun;
	import uglycode.explosions.actors.items.MachineGun;
	import uglycode.explosions.actors.items.Missile;
	import uglycode.explosions.actors.items.MissilePower;
	import uglycode.explosions.game.Registry;
	import uglycode.explosions.sound.Sequence;
	import uglycode.explosions.ui.BaseUI;
	import uglycode.explosions.ui.DebugGUI;
	import uglycode.explosions.ui.JngScreen;
	import uglycode.explosions.world.Backdrop;
	import uglycode.explosions.world.EnemySequence;
	import uglycode.explosions.world.Spawner;
	import uglycode.util.AssetManager;
	import uglycode.util.Logger;
	import uglycode.util.PoolObject;

	public class JetsGunsState extends StarlingState
	{
		private var assetManager:AssetManager;
		private var _ce:Explosions;
		private var _nape:Nape;
		public var ship:Ship;
		private var gui:DebugGUI;
		public var registry:Registry;
		private var loadedAtlases:int = 0;
		private var totalAtlases:int = 0;
		private var backdrop:Backdrop;
		
		// sorry, I know, static properties and testability and all that shit
		public var currentState:String = '';
		//private var misItem:DoublyLinkedListNode;
		//private var mv:Vector.<Missile>;
		private var cnt:uint;
		private var spawnerUfo1:Spawner;
		private var spawnerUfo2:Spawner;
		public var ui:BaseUI;
		public var powerManager:PowerManager;
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function JetsGunsState()
		{
			_ce = CitrusEngine.getInstance() as Explosions;
			super();
		}
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== public methods =================================================================
		 */


		override public function destroy():void {
			//destroy player
			remove(ship);
			ship.destroy();
			//ship.kill = true;
			
			//destroy pools
			this.registry.destroyAll(this);
			
			//destroy power manager
			
			//destroy enemies
			
			//destroy bullets
			
			//destroy particles
			
			//destroy the world
			
			//destroy background
			
			backdrop.destroy();
			
			//remove event listeners from Stage and elsewhere
			stage.removeEventListener(TouchEvent.TOUCH, onTouch);
			flash.system.System.pauseForGCIfCollectionImminent(0.01);
		}

		
		
		
		
		override public function update(timeDelta:Number):void {
			if (currentState == 'paused') {
				return;
			}
			super.update(timeDelta);
			if (backdrop === null) {
				trace('in main loop - backdrop not ready yet!');
				return;
			}
			if (currentState != 'ready') {
				return;
			}
			backdrop.updateBackground(timeDelta);
			if (ship !== null) {
				gui.setText('shot pools: '
					+ String(this.registry.getShotCount())
					+ ' --- plasma: '
					+ String(this.registry.evilPlasmaShotPool.length)
					+ ' --- enemy pools: ' 
					+ this.registry.getEnemyCount()
					+ ' --- Nape: ' + _nape.space.bodies.length + '/' + _nape.space.liveBodies.length);
			}
			
			this.registry.sequencer.update(timeDelta);
			//this.registry.updateSprites(this.view);
			if (cnt++ % 60 == 0) {
				this.registry.updatePools(timeDelta, this.view);
				if (cnt % 120 == 0) {
					this.checkGameOverState();
				}
			}
				//var tmpHead:DoublyLinkedListNode = this.registry.missilePool.head;
				//var toBeRemoved:Vector.<DoublyLinkedListNode> = new Vector.<DoublyLinkedListNode>;
				/*
				while (tmpHead != null) {
				if ((tmpHead.data as Missile).kill) {
				toBeRemoved.push(tmpHead);
				}
				tmpHead = tmpHead.next;
				}
				trace ('deleting: ' + toBeRemoved.length);
				for (var i:int = 0; i < toBeRemoved.length; i++) 
				{
				//remove(toBeRemoved[i].data);
				this.registry.missilePool.disposeNode(toBeRemoved[i]);
				}
				tmpHead = this.registry.missileImagePool.head;
				toBeRemoved = new Vector.<DoublyLinkedListNode>;
				while (tmpHead != null) {
				var cObj:CitrusObject = tmpHead.data as CitrusObject;
				if (cObj.kill == true) {
				toBeRemoved.push(tmpHead);
				}
				tmpHead = tmpHead.next;
				}
				trace ('images wo parents: ' + toBeRemoved.length);
				for (i = 0; i < toBeRemoved.length; i++) 
				{
				this.registry.missileImagePool.disposeNode(toBeRemoved[i]);
				}
				*/
			//this.registry.cleanupLeftovers();
		}
		
		
		

		private function partialDestroy():void
		{
			remove(ship);
			ship.destroy();
			
		}		

		
		
		public function restartLevel():void {
			this.registry.disposeAll(this);
			flash.system.System.pauseForGCIfCollectionImminent(0.2);
			
			ship = new Ship('hero', this.getShipDefaults());
			ship.parentState = this;
			add(ship);

			this.registry.stats.ePenisLength = 0;
			registry.sequencer.reset();
			this.currentState = 'ready';
		}


		

		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		override public function initialize():void {
			super.initialize();

			// set up Nape
			_nape = new Nape("nape");
			_nape.visible = true;
			add(_nape);
			
			// Load the atlas asynchronously
			// Don't do anything else until the atlas loads
			this.registry.assetManager.loadAtlas('temp', 'temp', onAtlasLoaded); totalAtlases++;
			this.registry.assetManager.loadAtlas('temp', 'backdrop', onAtlasLoaded); totalAtlases++;
		}
		
		
		
		private function onAtlasLoaded(atlas:TextureAtlas, group:String, atlasName:String):void {
			if (atlas == null)
			{
				Logger.log("Didn't load game atlas, major fail.");
				return;
			}
			Logger.log('loaded ' + group + ' / ' + atlasName);
			if (++loadedAtlases == totalAtlases) {
				initGame();
			}
		}
		
		
		
		private function initGame():void
		{
			trace('initGame');

			// once we have Nape, we can check&fill the registry
			registry.init(registry.HUGE);

			//init powerManager, but this won't work until we link it with UI
			this.powerManager = new PowerManager(this.registry);

			//init hero
			ship = new Ship('hero', this.getShipDefaults());
			ship.parentState = this;
			add(ship);
			//(ui.currentScreen as JngScreen).initShipUI(ship);

			// events and signals and such
			//stage.addEventListener(TouchEvent.TOUCH, onTouch);
			
			_ce.gameData.dataChanged.add(onDataChanged);
			
			//init UI
			gui = new DebugGUI();
			addChild(gui);
			ui = new BaseUI(this);
			addChild(ui);
			
			// second part of initing powerManager: link it with UI
			//this.powerManager.uiObject = ui.currentScreen;
			
			
			
			// set up controls
			initInput();
			

			//init guns
			
			
			//load song/enemy config
			//this is kinda weird, I should rethink the API. TODO maybe "sequencer.addPublicSequence(ISequencable, [enemy]sequence)" ?
			var enemySequence:EnemySequence = new EnemySequence();
			//*
			enemySequence.initSymmetricFifthSequence();
			//enemySequence.initRandomSequence();
			//enemySequence.initRandomSequence();
			//enemySequence.initRandomSequence();
			//enemySequence.initRandomSequence();
			
			enemySequence.initSymmetricFifthSequence();
			enemySequence.initSymmetricFifthSequence();
			enemySequence.initFleet10Sequence();
			enemySequence.initSymmetricFifthSequence();
			enemySequence.initRandomSequence();
			enemySequence.initDifficultSequence();
			// */
			//enemySequence.initDebugSequence();
			spawnerUfo1 = new Spawner(enemySequence, registry.ufo1Pool, uglycode.explosions.actors.Ufo1, this);
			//enemySequence.parentObject = spawnerUfo1;
			//registry.sequencer.seq.push(enemySequence);
			
			var enemySequence2:EnemySequence = new EnemySequence();
			enemySequence2.initSymmetricEightSequence();
			enemySequence2.initSymmetricEightSequence();
			enemySequence2.initSymmetricEightSequence();
			enemySequence2.initSymmetricFifthSequence();
			enemySequence2.initSymmetricEightSequence();
			enemySequence2.initSymmetricFifthSequence();
			enemySequence2.initFleet10Sequence();
			enemySequence2.parentObject = spawnerUfo2;
			spawnerUfo2 = new Spawner(enemySequence2, registry.ufo2Pool, uglycode.explosions.actors.Ufo2, this);
			
			
			
			//create the world
			backdrop = new Backdrop(this, _ce.registry.assetManager);
			backdrop.createBackground();
			
			
			//view.setupCamera(null, new MathVector(50, 50), new Rectangle(0, 0, int.MAX_VALUE, int.MAX_VALUE), new MathVector(.25, .05));
			view.setupCamera();
			
			
			//trash below
			//var hills:HillsManagingGraphics = new HillsManagingGraphics("hills", {sliceHeight:200, sliceWidth:70, currentYPoint:350, registration:"topLeft", view:_hillsTexture});
			//add(hills);
			
			// a bit of a hack, but whatever. "reset" might as well stand for "startup"
			registry.sequencer.reset();
			this.currentState = 'ready';
		}
		
		private function initInput():void
		{
			//your hero can listen to channel 1 this way.
			//ship.inputChannel = 1;
			
			var keyboard:Keyboard = CitrusEngine.getInstance().input.keyboard as Keyboard;
			//and here we can add actions to the keyboard, that will BLAH BLAH IGNORE THE FOLLOWING only be sent to channel 1.
			keyboard.addKeyAction('up', Keyboard.UP);
			keyboard.addKeyAction('down', Keyboard.DOWN);
			keyboard.addKeyAction('left', Keyboard.LEFT);
			keyboard.addKeyAction('right', Keyboard.RIGHT);
			keyboard.addKeyAction('fire', Keyboard.SPACE);
			
			var touchInput:TouchControls = new TouchControls('touchie');
			touchInput.registerShipControls(ship);
		}		
		
		
		private function getShipDefaults():Object
		{
			//TODO load from garage / config / armour / etc
			return {
				view: _ce.registry.assetManager.getImageFromAtlas('temp','temp','ship'),
					sequencer: registry.sequencer,
					weapons: [new MachineGun('mg1'), new FireballGun('fbg1')],
					powerManager: this.powerManager
			}
		}		



		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		private function onDataChanged():void
		{
			//update GUI
			//do shit
			//check if game over
			//whatever
		}		
		
		
		
		private function onTouch(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(this, TouchPhase.BEGAN);
			
			if (touch)
			{
				var localPos:Point = touch.getLocation(this.stage);
				/*if (touch.globalX < 100 && touch.globalY < 100) {
					trace('touched the corner, adding missiles');
					_temp_spawn_missiles(50);
				}
				else */
				if (touch.globalX < 80 && touch.globalY < 480 && touch.globalY > 400) {
					trace('reclaiming all pool items');
					this.registry.disposeAll(this);
					//_temp_kill_missiles();
				}
				/*if (touch.globalX < ship.x) {
					
				}*/
				//trace("X Touched object at position: " + touch.globalX + 'x' + touch.globalY+ ' or ' + localPos);
			}
			
		}
		
		
		public function checkGameOverState():void {
			// LTTODO move this into gameData and move this check to onDataChanged()
			// [done] check if player is dead
			// [done] check if boss is killed, if he existed
			// TODO check if battery is low, but not too often
			var allEnemyQueuesEmptyOrIrrelevant:Boolean = true;
			for each (var seq:Sequence in registry.sequencer.seq)
			{
				if (seq is EnemySequence) {
					if ((seq as EnemySequence).stopSpawningAfterQueue === true) {
						allEnemyQueuesEmptyOrIrrelevant &&= (seq as EnemySequence).empty;
					}
				}
			}
			var allBossesDown:Boolean = true;
			var allEnemiesGone:Boolean = registry.enemyPoolsAreEmpty();
			var playerDead:Boolean = ship.dead;
			
			if (playerDead || allEnemyQueuesEmptyOrIrrelevant && allEnemiesGone) {
				if (playerDead) {
					// or other LOSE conditions
					this.currentState = 'lose';
				}
				else {
					this.currentState = 'win';
				}
				trace ("The game should end now.");
				ui.switchScreen('showGameOver');
				this.partialDestroy();
				//this.destroy();
			}
		}

		
		
		
		/**
		 * =================================================================== temporary bullshit =============================================================
		 */
		/*
		private function _temp_spawn_missiles(count:int):void {
			//mv = new Vector.<Missile>(300);
			for (var a:int=0; a < count; a++) {
				misItem = this.registry.missilePool.create({x: 150 + (Math.random()*50), y: 220, speed: 40 + (Math.random()*20), angle:  (Math.PI*2)+(Math.random()*0.1),
					blastRadius: 120, fuseDuration: 3000 + (Math.random() * 800),
					useForce: false, view: this.registry.assetManager.getImageFromAtlas('temp', 'temp', 'bolt_a_01')});
				//this.registry.missileImagePool.create(misItem.data.view);
				//add(this.registry.missileImagePool.create(misItem.data).data);
				add(misItem.data);
				//mv[a] = misItem.data;
				//add(this.registry.missileImagePool.create(mv[a]).data);
				//add(mv[a]);
			}
		}
		
		
		
		private function _temp_kill_missiles():void
		{
			this.registry.destroyAll(this);
		}		
		*/
	}








	

}























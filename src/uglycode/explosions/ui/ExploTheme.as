package uglycode.explosions.ui
{
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import citrus.core.CitrusEngine;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.ProgressBar;
	import feathers.display.Scale3Image;
	import feathers.skins.Scale9ImageStateValueSelector;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale3Textures;
	import feathers.textures.Scale9Textures;
	import feathers.themes.AzureMobileTheme;
	
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	
	import uglycode.explosions.Explosions;
	
	public class ExploTheme extends AzureMobileTheme
	{
		public static const SHIELD_PROGRESS:String = "shield-progressbar";
		public static const HEALTH_PROGRESS:String = "health-progressbar";
		protected var progressBarFillSkinTexturesCyan:Scale3Textures;
		protected var progressBarFillSkinTexturesRed:Scale3Textures;
		
		
		public static const ICON_MISSILE:String = "missile-icon";
		protected var missile_buttonUpSkinTextures:Scale9Textures;

		
		
		public function ExploTheme(root:DisplayObjectContainer, scaleToDPI:Boolean=true)
		{
			super(root, scaleToDPI);
		}
		
		
		override protected function initialize():void
		{
			super.initialize();
			
			//progress bars - health, shield
			this.progressBarFillSkinTexturesCyan = new Scale3Textures(this.atlas.getTexture("progress-bar-fill-skin-cyan"), PROGRESS_BAR_SCALE_3_FIRST_REGION, PROGRESS_BAR_SCALE_3_SECOND_REGION, Scale3Textures.DIRECTION_HORIZONTAL);
			this.progressBarFillSkinTexturesRed = new Scale3Textures(this.atlas.getTexture("progress-bar-fill-skin-red"), PROGRESS_BAR_SCALE_3_FIRST_REGION, PROGRESS_BAR_SCALE_3_SECOND_REGION, Scale3Textures.DIRECTION_HORIZONTAL);
			setInitializerForClass( ProgressBar, shieldBarInit, SHIELD_PROGRESS );
			setInitializerForClass( ProgressBar, healthBarInit, HEALTH_PROGRESS );
			
			//missile button
			//this.buttonUpSkinTextures = new Scale9Textures(this.atlas.getTexture("button-up-skin"), BUTTON_SCALE_9_GRID);
			//this.setInitializerForClass( Button, missile_buttonInitializer, ICON_MISSILE );
			this.setInitializerForClass( Button, powerButtonInitializer, 'powerButton' );
			
			// this was an experiment to fuck up bottongroup, but screw them, YOU ARE NOT THE FATHER OF ME
			// this.setInitializerForClass( Button, new Function(), ButtonGroup.DEFAULT_CHILD_NAME_BUTTON );
			
		}
		
		private function powerButtonInitializer(button:Button):void
		{
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(this.bitmapFont, 24, 0x323232, TextFormatAlign.CENTER);
			button.iconPosition = Button.ICON_POSITION_MANUAL;
			button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			// HOW THE FUCK DOES THIS WORK, FEATHERS? WHY ARE YOU TORTURING ME? WHY ALIGHING IT LEFT ACTUALLY CENTERS IT? WHY DO YOU HATE LOGIC?
			button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			//throw new Error ('derp-recated');
			//button.defaultSkin = new Image((CitrusEngine.getInstance() as Explosions).registry.assetManager.getTextureFromAtlas('temp','temp','icon-missile'));
			/*const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = buttonUpSkinTextures;
			skinSelector.defaultSelectedValue = buttonDownSkinTextures;
			skinSelector.imageProperties =
				{
					width: 48,
					height: 48,
					textureScale: 1
				};
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.paddingTop = button.paddingBottom = 0;
			button.paddingLeft = button.paddingRight = 0;
			button.gap = 0;
			button.minWidth = button.minHeight = 48;
			button.minTouchWidth = button.minTouchHeight = 48;
			*/
		}
		
		
		private function shieldBarInit(bar:ProgressBar):void
		{
			const backgroundSkin:Scale3Image = new Scale3Image(progressBarBackgroundSkinTextures, this.scale);
			backgroundSkin.width = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 264 : 24) * this.scale;
			backgroundSkin.height = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 24 : 264) * this.scale;
			bar.backgroundSkin = backgroundSkin;
			
			const backgroundDisabledSkin:Scale3Image = new Scale3Image(progressBarBackgroundDisabledSkinTextures, this.scale);
			backgroundDisabledSkin.width = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 264 : 24) * this.scale;
			backgroundDisabledSkin.height = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 24 : 264) * this.scale;
			bar.backgroundDisabledSkin = backgroundDisabledSkin;
			
			const fillDisabledSkin:Scale3Image = new Scale3Image(progressBarFillDisabledSkinTextures, this.scale);
			fillDisabledSkin.width = 24 * this.scale;
			fillDisabledSkin.height = 24 * this.scale;
			bar.fillDisabledSkin = fillDisabledSkin;
			
			const fillSkin:Scale3Image = new Scale3Image(progressBarFillSkinTexturesCyan, this.scale);
			fillSkin.width = 24 * this.scale;
			fillSkin.height = 24 * this.scale;
			bar.fillSkin = fillSkin;
		}
		
		
		
		private function healthBarInit(bar:ProgressBar):void
		{
			const backgroundSkin:Scale3Image = new Scale3Image(progressBarBackgroundSkinTextures, this.scale);
			backgroundSkin.width = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 264 : 24) * this.scale;
			backgroundSkin.height = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 24 : 264) * this.scale;
			bar.backgroundSkin = backgroundSkin;
			
			const backgroundDisabledSkin:Scale3Image = new Scale3Image(progressBarBackgroundDisabledSkinTextures, this.scale);
			backgroundDisabledSkin.width = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 264 : 24) * this.scale;
			backgroundDisabledSkin.height = (bar.direction == ProgressBar.DIRECTION_HORIZONTAL ? 24 : 264) * this.scale;
			bar.backgroundDisabledSkin = backgroundDisabledSkin;
			
			const fillDisabledSkin:Scale3Image = new Scale3Image(progressBarFillDisabledSkinTextures, this.scale);
			fillDisabledSkin.width = 24 * this.scale;
			fillDisabledSkin.height = 24 * this.scale;
			bar.fillDisabledSkin = fillDisabledSkin;
			
			const fillSkin:Scale3Image = new Scale3Image(progressBarFillSkinTexturesRed, this.scale);
			fillSkin.width = 24 * this.scale;
			fillSkin.height = 24 * this.scale;
			bar.fillSkin = fillSkin;
		}
	}
}
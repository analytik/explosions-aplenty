package uglycode.explosions.ui
{
	import flash.system.Capabilities;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	internal final class Helper extends Sprite		
	{
		public function Helper()
		{
			//
		}
		
		public function SarahNorthwaysThingy(base:MovieClip):void {
			
			var dpi:Number = Capabilities.screenDPI; 
			var dpWide:Number = stage.fullScreenWidth * 160 / dpi; 
			var inchesWide:Number = stage.fullScreenWidth / dpi;
			
			var serverString:String = unescape(Capabilities.serverString); 
			var reportedDpi:Number = Number(serverString.split("&DP=", 2)[1]);
			
			var guiSize:Rectangle = new Rectangle(0, 0, 1024, 600);
			var deviceSize:Rectangle = new Rectangle(0, 0,
				Math.max(stage.fullScreenWidth, stage.fullScreenHeight),
				Math.min(stage.fullScreenWidth, stage.fullScreenHeight));
			
			var appScale:Number = 1;
			var appSize:Rectangle = guiSize.clone();
			var appLeftOffset:Number = 0;
			
			// if device is wider than GUI's aspect ratio, height determines scale
			if ((deviceSize.width/deviceSize.height) > (guiSize.width/guiSize.height)) {
				appScale = deviceSize.height / guiSize.height;
				appSize.width = deviceSize.width / appScale;
				appLeftOffset = Math.round((appSize.width - guiSize.width) / 2);
			} 
			// if device is taller than GUI's aspect ratio, width determines scale
			else {
				appScale = deviceSize.width / guiSize.width;
				appSize.height = deviceSize.height / appScale;
				appLeftOffset = 0;
			}
			
			// scale the entire interface
			base.scale = appScale;
			
			// map stays at the top left and fills the whole screen
			base.map.x = 0;
			
			// menus are centered horizontally
			base.menus.x = appLeftOffset;
			
			// crop some menus which are designed to run off the sides of the screen
			base.scrollRect = appSize;
		}
	}
}
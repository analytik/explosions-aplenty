package uglycode.explosions.ui
{
	import flash.utils.Dictionary;
	
	import feathers.core.FeathersControl;
	
	/**
	 * TODO extend FeathersControl, add to stage? Mimic ButtonGroup API a little?
	 */
	public class ButtonSlot
	{
		public var x:int;
		public var currentX:int;
		public var y:int;
		public var currentY:int;
		public var verticalMode:Boolean = false;
		public var name:String;
		public var marginX:int = 12;
		public var marginY:int = 12;
		public var buttons:Dictionary = new Dictionary;
		//todo maxwidth, etc etc
		//todo gridmode etc
		
		public function ButtonSlot(x:int, y:int, slotName:String)
		{
			this.currentX = this.x = x;
			this.currentY = this.y = y;
			this.name = slotName;
		}
		
		//public function add button
		//gets button width, changes currentX/currentY
		public function addButton(name:String, something:FeathersControl):void {
			something.validate();
			if (!this.verticalMode) { 
				something.x = this.currentX;
				something.y = this.y;
				this.currentX += something.width + this.marginX;
			}
			else {
				something.y = this.currentY;
				something.x = this.x;
				this.currentY += something.height + this.marginY;
			}
			this.buttons[name] = something;
		}
		
		//todo removebutton, resort, etc
		public function removeButton(name:String = null, something:FeathersControl = null):void {
			something = this.getButton(name,something);
			if (something == null) return;
			if (!this.verticalMode) { 
				this.currentX -= (something.width + this.marginX);
			}
			else {
				this.currentY -= (something.height + this.marginY);
			}
		}
		
		public function getButton(name:String = null, something:FeathersControl = null):FeathersControl {
			if (!name && !something) {
				throw new Error('Please provide at least one parameter.');
			}
			if (!something) {
				if (!this.buttons.hasOwnProperty(name)) {
					trace ('cannot remove button ' + name);
					return null;
				}
				something = this.buttons[name];
			}
			return something;
		}
		
	}
}
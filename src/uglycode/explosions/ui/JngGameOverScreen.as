package uglycode.explosions.ui
{
	
	import citrus.core.CitrusEngine;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	import uglycode.explosions.game.JetsGunsState;
	
	
	public class JngGameOverScreen extends Screen
	{
		private var _button:Button;
		private var _backButton:Button;
		private var _header:Header;
		private var _scoreButton:Button;
		
		
		
		public function JngGameOverScreen()
		{
			super();
		}
		
		

		override protected function initialize():void
		{
			var state:String = (CitrusEngine.getInstance().state as JetsGunsState).currentState;
			
			this._button = new Button();
			this._button.label = "[to main menu]\nThis does\nnothing at all!";
			this._button.isToggle = false;
			this._button.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
			this._button.verticalAlign = Button.VERTICAL_ALIGN_MIDDLE;
			this._button.iconPosition = Button.ICON_POSITION_LEFT;
			this._button.iconOffsetX = 0;
			this._button.iconOffsetY = 0;
			this._button.x = 500;
			this._button.y = 250;
			this._button.width = 264 * this.dpiScale;
			this._button.height = 120 * this.dpiScale;
			this._button.addEventListener(Event.TRIGGERED, button_triggeredHandler);
			this.addChild(this._button);
			
			this._backButton = new Button();
			this._backButton.label = "Restart level";
			this._backButton.addEventListener(Event.TRIGGERED, backButton_triggeredHandler);
			
			this._scoreButton = new Button();
			_scoreButton.label = "Final score: " + (CitrusEngine.getInstance().state as JetsGunsState).registry.stats.ePenisLength;
			addChild(_scoreButton);
			_scoreButton.validate();
			_scoreButton.x = (starling.core.Starling.current.stage.stageWidth/2) - (_scoreButton.width/2);
			_scoreButton.y = (starling.core.Starling.current.stage.stageHeight/3); //33%
			
			this._header = new Header();
			if (state == 'gameoverwon') {
				this._header.title = "Nice job! You murdered them!";
			}
			else {
				this._header.title = "Game over, man! Unless you try again.";
			}
			this._header.width = starling.core.Starling.current.stage.stageWidth;
			this._header.validate();
			this.addChild(this._header);
			this._header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			
			// handles the back hardware key on android
			this.backButtonHandler = this.onBackButton;
		}
		
		private function onBackButton():void
		{
			//(CitrusEngine.getInstance().state as JetsGunsState).currentState = oldState;
			(CitrusEngine.getInstance().state as JetsGunsState).restartLevel();
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function button_triggeredHandler(event:Event):void
		{
			trace("button triggered.")
		}
		
		private function backButton_triggeredHandler(event:Event):void
		{
			this.onBackButton();
		}
		
		
		
	
	
	}
}
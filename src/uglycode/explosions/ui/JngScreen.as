package uglycode.explosions.ui
{
	import flash.utils.Dictionary;
	
	import citrus.core.CitrusEngine;
	import citrus.core.StarlingState;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.ProgressBar;
	import feathers.controls.Screen;
	import feathers.core.FeathersControl;
	import feathers.data.ListCollection;
	import feathers.skins.StandardIcons;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.actors.items.Power;
	import uglycode.explosions.game.JetsGunsState;
	import uglycode.explosions.game.PowerManager;
	
	//what the fuck is this piece of fucking flex shit
	//[Event(name="showOptions1",type="starling.events.Event")]
	
	public class JngScreen extends Screen
	{
		
		//private var _header:Header;
		private var _list:List;
		private var _backButton:Button;
		public var healthBar:ProgressBar;
		public var shieldBar:ProgressBar;
		public var missileButton:Button;
		public var onSpecialAbility:Signal;
		
		// TODO DEPRECATE, replace w signals
		private var ship:Ship;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function JngScreen()
		{
			super();
		}
		

		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		public function getParentState():StarlingState {
			return (this.owner.parent as BaseUI).parentState;
		}
		
		/* deprecated crap
		public function get positions():Array {
			return [ PowerManager.SLOT_WEAPONS ];
		}
		*/
		
		/*
		public function get powerButtons():Dictionary
		{
			return _powerButtons;
		}
		
		public function set powerButtons(value:Dictionary):void
		{
			_powerButtons = value;
		}
		*/

		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * UI for Jets&Guns game state. We want to show
		 * - Pause button (?) - upper right corner
		 * - Health & Shield - upper left/middle
		 * - [later] weapon mode / autofire buttons - bottom left
		 * - special skills (missiles, nukes, boosters, air strike, whatever) - bottom
		 * - [later] targetted enemy HP - upper right
		 */
		
		override protected function initialize():void
		{
			this._backButton = new Button();
			_backButton.label = "Pause";
			_backButton.horizontalAlign = Button.HORIZONTAL_ALIGN_RIGHT;
			_backButton.addEventListener(Event.TRIGGERED, goAheadAndShowOptions);
			addChild(_backButton);
			_backButton.validate();
			_backButton.x = starling.core.Starling.current.stage.stageWidth - _backButton.width;
			/*
			this._list = new List();
			this._list.dataProvider = new ListCollection(
			[
			{ label: "Options", event: 'showOptions' },
			{ label: "Missile", event: 'fireMissile' },
			]);
			this._list.itemRendererProperties.labelField = "label";
			this._list.itemRendererProperties.accessorySourceFunction = accessorySourceFunction;
			this._list.addEventListener(Event.CHANGE, list_changeHandler);
			this.addChild(this._list);
			*/
			
				
			/**
			 * TODO CLUMSY REWRITE, ABSTRACT
			 */
			//for each (var s:String in this.positions) {}
			
			

			this.initShipUI((getParentState() as JetsGunsState).ship);
			
			
			// handles the back hardware key on android
			this.backButtonHandler = this.onBackButton;
		}
		
		override protected function screen_removedFromStageHandler(event:Event):void {
			trace ('sup bitches, Im being removed');
			this.ship.powerManager.removePowerButtons();
		}
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		
		public function goAheadAndShowOptions(event:Event):void {
			this.dispatchEventWith('showOptions');
		}
		


		
		
		/**
		 * TODO rewrite with signals
		 */
		public function initShipUI(ship:Ship):void {
			trace ('INITING SHIP UI');
			this.ship = ship;
			this.healthBar = new ProgressBar();
			healthBar.minimum = 0;
			healthBar.nameList.add( ExploTheme.HEALTH_PROGRESS );
			healthBar.maximum = ship.defaultHitPoints;
			healthBar.value = ship.hitPoints;
			healthBar.x = 10; healthBar.y = 10;
			healthBar.alpha = 0.7;
			addChild(healthBar);
			healthBar.validate();
			this.shieldBar = new ProgressBar();
			shieldBar.minimum = 0;
			shieldBar.nameList.add( ExploTheme.SHIELD_PROGRESS );
			shieldBar.maximum = ship.defaultShield;
			shieldBar.value = ship.shield;
			shieldBar.x = 20 + healthBar.width;
			shieldBar.y = 10;
			shieldBar.alpha = 0.7;
			shieldBar.validate();
			addChild(shieldBar);
			
			//find power manager
			this.ship.powerManager.uiObject = this;
		}
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		private function backButton_triggeredHandler(event:Event):void
		{
			// TODO if android button pressed, pause+show Y/N screen and quit/resume
			trace ('back button pressed');
			this.dispatchEventWith(Event.CHANGE);
		}
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		override protected function draw():void
		{
			//this._header.width = this.actualWidth;
			//this._header.validate();
			/*
			I think this is useless
			if (!(getParentState() as JetsGunsState).ship) return;
			healthBar.value = (getParentState() as JetsGunsState).ship.hitPoints;
			trace ('[in JngScreen.draw()] hit points: ' + healthBar.value);
			*/
			if (!this._list) {
				return;
			}
			
			this._list.y = 150;
			this._list.width = this.actualWidth;
			this._list.height = this.actualHeight - this._list.y;
		}
		
		
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
		
		private function launchMissile():void
		{
			(CitrusEngine.getInstance() as Explosions).sound.playSound('missile_a', 1, 1);
			trace('touched missile');
		}		
		
		
		
		private function accessorySourceFunction(item:Object):Texture
		{
			return StandardIcons.listDrillDownAccessoryTexture;
		}
		
		
		
		private function list_changeHandler(event:Event):void
		{
			const eventType:String = this._list.selectedItem.event as String;
			this.dispatchEventWith(eventType);
		}
		
		
		
		private function onBackButton():void
		{
			trace ('in-game or Android back button pressed.');
			this.dispatchEventWith(Event.COMPLETE);
		}

		
		

		
		
		
		
		
		
	}
}
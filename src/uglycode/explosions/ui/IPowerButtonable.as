package uglycode.explosions.ui
{
	import flash.utils.Dictionary;
	
	import feathers.core.FeathersControl;
	
	import org.osflash.signals.Signal;
	
	import starling.textures.Texture;
	import uglycode.explosions.actors.items.Power;

	public interface IPowerButtonable
	{
		/**
		 * human-readable string identifiers for different button slots (weapons, buildings, upgrades, whatever)
		 */
		//function get positions():Array;
		//function get powerButtons():Dictionary; //<FeathersControl>;
		//function get buttonSlots():Vector.<ButtonSlot>;
		
		function addPowerButton(ident:String, slot:String, texture:Texture, pushedHandler:Function, updateSignal:Signal):FeathersControl;
		function removePowerButton(ident:String, slot:String):void;
		
		
	}
}
package uglycode.explosions.ui
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	public class DebugGUI extends Sprite
	{
		private var score:TextField;
		
		public function DebugGUI()
		{
			score = new TextField(800, 60, "0", "Verdana", 16, 0x0033FF);
			score.hAlign = "right";
			addChild(score);
		}
		
		public function setText(meh:String):void
		{
			score.text = meh;
		}
	}
}
package uglycode.explosions.ui
{
	import citrus.core.StarlingState;
	
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.Screen;
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.motion.transitions.ScreenSlidingStackTransitionManager;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import uglycode.explosions.game.JetsGunsState;
	
	public class BaseUI extends Sprite
	{
		private var _theme:ExploTheme;
		private var _navigator:ScreenNavigator;
		private var _transitionManager:ScreenSlidingStackTransitionManager;
		private var _debugMode:Boolean = true;
		public var parentState:StarlingState;

		public function BaseUI(citrusState:StarlingState)
		{
			super();
			parentState = citrusState;
			_debugMode = Starling.current.showStats;
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		
		
		/**
		 * JNG states:
		 *  - game (+ options, game over) [Alpha 1]
		 *  - garage (equipment, next level) [Alpha 2]
		 *  - main menu [Alpha 3]
		 */
		private function addedToStageHandler():void
		{
			this._theme = new ExploTheme(this.stage);
			//this dirty fucking trick will remove the default skin completely, instead of having to create one method for every different texture
			//_theme.setInitializerForClass( Button, new Function(), "powerButton" );
			

			
			/**
			 * UI screens for Jets&Guns game state. We need
			 *  - game screen
			 *  - pause screen [options, sound, quit, time, stats, etc]
			 *  - game over you suck + game over you're a winner
			 */
			if (parentState is JetsGunsState) {
				this._navigator = new ScreenNavigator();
				this.addChild(this._navigator);
				// ident of the screen => its object (screen Class, params)
				this._navigator.addScreen('JNG_game', new ScreenNavigatorItem(JngScreen,
					{
						// eventName => ident of the screen
						showOptions: 'showOptions'
					}));
				this._navigator.addScreen('showOptions', new ScreenNavigatorItem(JngOptionsScreen,
					{
						complete: 'JNG_game'
					}));
				this._navigator.addScreen('showGameOver', new ScreenNavigatorItem(JngGameOverScreen,
					{
						complete: 'JNG_game' // return to game on "restart level" option
					}));
				
				this._navigator.showScreen('JNG_game');
				
				this._transitionManager = new ScreenSlidingStackTransitionManager(this._navigator);
				this._transitionManager.duration = 0.4;
			}
			//else if parentState is GarageState { ... some other UI ... }
		}
		
		
		
		public function switchScreen(newScreen:String):void {
			this._navigator.showScreen(newScreen);
		}
		
		
		public function get currentScreen():Screen {
			return this._navigator.activeScreen as Screen;
		}
		
		
		/**
		 * todo replace with signals
		 */
		public function updateHitPoints(hp:Number):void {
			if (_navigator.activeScreen is JngScreen) {
				(_navigator.activeScreen as JngScreen).healthBar.value = hp;
			}
		}
		public function updateShield(hp:Number):void {
			if (_navigator.activeScreen is JngScreen) {
				(_navigator.activeScreen as JngScreen).shieldBar.value = hp;
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
package uglycode.explosions.actors
{
	import nape.geom.Vec2;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.items.EvilRedGun;

	public class Ufo2 extends Enemy
	{
		public static var viewType:String = 'image';
		public static var viewGroup:String = 'temp';
		public static var viewAtlas:String = 'temp';
		public static var viewSprite:String = 'ufo2';
		
		
		
		public function Ufo2(name:String, params:Object=null)
		{
			super(name, params);
			
			weapons.push(new EvilRedGun('epg' + int(Math.random()*48000)));
			weapons[0].parentShip = this;
			
		}
		
		override public function initialize(poolObjectParams:Object=null):void {
			super.initialize(poolObjectParams);
			y = (Math.cos((_ce as Explosions).registry.sequencer.currentBeat)*250),
			this.hitPoints = 10 + (level * 8);
		}
		
		
		public static function getSpawnerOptions():Object {
			return {
				x: 850, 
				y: 190,
				speed: -60, //TODO plus ship's level?
				angle:  0,
				rotation: 0,
				score: 15
			};
		}
		
		override public function update(timeDelta:Number):void
		{
			//super.update(timeDelta);
			//sView.scaleX = 1;
			
			//var newy:Number = Math.max(Math.min(bottomBoundary, startingY + Math.sin(ownSequence++)*amplitude), topBoundary);
			var newy:Number = Math.round(Math.max(Math.min(bottomBoundary, startingY + ( (Math.tan(x/40) + Math.cos(x/40) ) * (amplitude/2) ) ), topBoundary) - y);
			if (pausedAmplitude === false) {
				if (++pauseTimer > 80 && Math.random()*100 < 3) {
					pauseTimer = 0;
					pausedAmplitude = true;
				}
				_body.velocity = Vec2.get(speed, newy);
				//var newy:Number = startingY + Math.sin((x / 100)) * (amplitude / 2);
				//trace (newy + '      ' + Math.sin(x/10)*(amplitude/2));
			}
			else {
				if (++pauseTimer > 20) {
					pausedAmplitude = false;
					pauseTimer = 0;
				}
				_body.velocity = Vec2.get(speed * 0.7, newy);
				fire();
			}
			
			updateWeapons(timeDelta);
			//updateAnimation();
			
			if (x < -100) {
				kill = true;
			}
			
			if (hitPoints <= 0) {
				(_ce as uglycode.explosions.Explosions).registry.stats.ePenisLength += 30;
				(_ce as uglycode.explosions.Explosions).registry.stats.enemiesKilled['_small'] += 1;
				kill = true;
			}
			
			// update the animation
			var prevAnimation:String = _animation;
		}
		
		
		
		override protected function getDeathSound():String
		{
			return 'expl_a_1'; 
		}	

	}
}
package uglycode.explosions.actors
{
	import citrus.core.CitrusEngine;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.items.EvilPlasmaGun;
	
	public class Ufo1 extends Enemy
	{
		public static var viewType:String = 'image';
		public static var viewGroup:String = 'temp';
		public static var viewAtlas:String = 'temp';
		public static var viewSprite:String = 'ufo1';
		
		
		public function Ufo1(name:String, params:Object=null)
		{
			super(name, params);
			
			weapons.push(new EvilPlasmaGun('epg' + int(Math.random()*48000)));
			weapons[0].parentShip = this;
			
		}
		
		override public function initialize(poolObjectParams:Object=null):void {
			super.initialize(poolObjectParams);
			y = (Math.sin((_ce as Explosions).registry.sequencer.currentBeat)*150), //todo move to init for pooling
			this.hitPoints = 15 + (level * 10);
		}
		
		public static function getSpawnerOptions():Object {
			return {
				x: 850, 
				y: 100 + (((CitrusEngine.getInstance() as Explosions).registry.sequencer.currentBeat % 30) * 10),
				speed: -60, //TODO plus ship's level?
				angle:  0,
				rotation: 0,
				score: 20
			};
		}
		
		
		override protected function getDeathSound():String
		{
			return 'expl_a_2';
		}
		
	}
}
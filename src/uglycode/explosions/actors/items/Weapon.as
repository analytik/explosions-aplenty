package uglycode.explosions.actors.items
{
	import citrus.core.CitrusEngine;
	import citrus.core.CitrusObject;
	
	import flash.utils.getTimer;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.sound.ISequencable;
	import uglycode.explosions.sound.Sequence;

	/**
	 * Weapon manages the rhytm of shooting, can later have ammo, heat, weight, price, etc
	 */
	public class Weapon extends CitrusObject implements ISequencable
	{
		protected var _sequence:Sequence;
		protected var _ce:Explosions;
		/**
		 * Upgrade level of the weapon, 1-10
		 */
		protected var level:uint = 1;
		protected var cooldown:Number = 1500; //milliseconds
		protected var lastFired:uint;
		//protected var cooldownCounter:Number = 0;
		protected var damage:Number = 10; //this should be passed to shot afterwards
		
		/**
		 * Obviously, only set & play this if you don't use sequencer to fire.
		 */
		protected var unsequencedFireSound:String;


		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */

		public function Weapon(name:String, params:Object = null)
		{
			super(name, params);
			_ce = CitrusEngine.getInstance() as Explosions;
			initSound();
		}

		

		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		public function get sequence():Sequence
		{
			return _sequence;
		}
		
		public function set sequence(value:Sequence):void
		{
			_sequence = value;
		}
		
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		protected function initSound():void
		{
			sequence = new Sequence();
			sequence.initRandomSequence();
			sequence.parentObject = this;
			//only do this if you want sequencer to trigger the weapon, i.e. not the player firing!!!
			//_ce.registry.sequencer.seq.push(sequence);
		}
		
				
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		/**
		 * The current state calls update every tick. This is where all your per-frame logic should go. Set velocities, 
		 * determine animations, change properties, etc. 
		 * @param timeDelta This is a ratio explaining the amount of time that passed in relation to the amount of time that
		 * was supposed to pass. Multiply your stuff by this value to keep your speeds consistent no matter the frame rate. 
		 */		
		override public function update(timeDelta:Number):void
		{
			//cooldownCounter += timeDelta * 1000;
			trace("Override me, fuckass");
		}
		
		
		
		/**
		 * Seriously, don't forget to release your listeners, signals, and physics objects here. Either that or don't ever destroy anything.
		 * Your choice.
		 */		
		override public function destroy():void
		{
			_initialized = false;
		}
		
		public function fire(param:uint = 1):void {
			trace("Override me, fuckass, don't forget to reset cooldown");
		}
		

		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		/**
		 * Implement game logic (cooldown, ammo, generator power, etc) here
		 */
		public function canFire():Boolean {
			return ((getTimer() - lastFired) > cooldown)
		}
		
		
		public function getTransparencyLevel():Number {
			return 0.4 + (level * 0.6); 
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
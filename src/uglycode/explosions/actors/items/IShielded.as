package uglycode.explosions.actors.items
{
	public interface IShielded
	{
		function get shieldRechargeRate():Number;
		function get shieldRechargeDelay():Number;
		function get shieldCapacity():Number;
		function get shieldStatus():String; //charging, depleted, charged, halted
		function get shieldResistances():Array; // elemental damage - percentual resistance
	}
}
package uglycode.explosions.actors.items
{
	import citrus.core.CitrusEngine;
	import citrus.datastructures.DoublyLinkedListNode;

	public class MissilePower extends Power
	{
		public function MissilePower()
		{
			this.cooldown = 8;
			this.energyNeeded = 0;
			this.fireSound = 'missile_a';
			this.viewGroup = 'temp';
			this.viewAtlas = 'temp';
			this.viewSprite = 'icon-missile';
			this.name = 'missile';
			super();
		}
		
		// this is target for the Signal coming from UI
		override public function fire():void {
			if (this.totalCooldown == 0) {
				//trace ('FYEAH ACTIVATE ' + this);
				CitrusEngine.getInstance().sound.playSound(this.fireSound, 1, 1);
				this.currentCooldown = cooldown;
				onChange.dispatch(slot, name, totalCooldown);
				
				var misItem:DoublyLinkedListNode = registry.missilePool.create({
					x: parentShip.x+33, // TODO get weapon slot x,y 
					y: parentShip.y+8,  // TODO ...
					speed: 180 + (Math.random()*20) + (parentShip.velocity[0]/2),
					angle:  0.5,
					rotation: 0.5,
					fuseDuration: 2000 + (Math.random() * 800),
					view: registry.assetManager.getMovieFromAtlas('temp', 'temp', 'mini-missile')
				});
				//add to the scene, yo
				parentShip.parentState.add(misItem.data);

			}
			//else { trace ('sorry medear, but you need to cool down your ' + this); }
		}
	}
}
package uglycode.explosions.actors.items
{
	import citrus.objects.NapePhysicsObject;
	
	import flash.display.MovieClip;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.geom.Vec2;
	import nape.shape.Circle;
	
	import org.osflash.signals.Signal;
	
	public class FireballGunShot extends Shot
	{
		

		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function FireballGunShot(name:String, params:Object=null)
		{
			super(name, params);
			onExplode = new Signal(FireballGunShot, NapePhysicsObject);
		}
		
		

		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		override public function get rotation():Number
		{
			return angle;
		}
		
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			updateAnimation();
		}
		
		
		
		override public function destroy():void
		{
			onExplode.removeAll();
			//_fixture.removeEventListener(ContactEvent.BEGIN_CONTACT, handleBeginContact);
			clearTimeout(_fuseDurationTimeoutID);
			
			super.destroy();
		}
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * This method will often need to be overriden to customize the Nape shape object.
		 * The PhysicsObject creates a rectangle by default if the radius it not defined, but you can replace this method's
		 * definition and instead create a custom shape, such as a line or circle.
		 */	
		override protected function createShape():void {
			//_shape = new Circle(4.5, null, _material);
			//_body.shapes.clear();
			var p:Circle = new Circle(4.5, null, _material);
			// this belongs to createFilter() which is in Shot/EnemyShot class
			//p.filter.collisionGroup = 2; //in group 0x00000002
			//p.filter.collisionMask = ~2; //collide with everything in groups 0xfffffffd
			_body.shapes.add(p);
			//_body.scaleShapes(0.999, 1.001);
			_body.translateShapes(Vec2.weak(0,0));
		}
		
		
		
		override protected function createConstraint():void {
			super.createConstraint();
			
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, SHOT, CbType.ANY_BODY.excluding([SHOT, ENEMYSHOT]), handleBeginContact);
			//_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, SHOT, CbType.ANY_BODY.excluding(SHOT), handleEndContact);
			_body.cbTypes.add(SHOT);
			_body.space.listeners.add(_beginContactListener);
			//_body.space.listeners.add(_endContactListener);
		}
		
		
		
		/**
		 * =================================================================== handlers ===========================================================================
		 */

		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		

		
	}
}


package uglycode.explosions.actors.items
{
	import citrus.datastructures.DoublyLinkedListNode;
	
	import flash.utils.getTimer;
	
	import uglycode.explosions.actors.Enemy;
	import uglycode.explosions.sound.Pattern;
	import uglycode.explosions.sound.Sequence;
	
	
	
	public class EvilPlasmaGun extends Weapon
	{
		public var parentShip:Enemy;
		
		public function EvilPlasmaGun(name:String, params:Object = null)
		{
			super(name, params);
			cooldown = 2200;
		}
		
		override public function fire(ignored:uint = 0):void {
			//_ce.sound.playSound("gun", 1, 0);
			if (canFire()) {
				var misItem:DoublyLinkedListNode = _ce.registry.evilPlasmaShotPool.create({
					x: parentShip.x, // TODO get weapon slot x,y 
					y: parentShip.y+14,  // TODO ...
					speed: Math.floor(-180 + (Math.random()*20) + (parentShip.velocity[0]/2)), //floor it to make it poolable
					angle:  (Math.PI*2)-0.02 + (Math.random()*0.05),
					rotation: 0,
					fuseDuration: 2000 + (Math.random() * 800),
					view: _ce.registry.assetManager.getMovieFromAtlas('temp', 'temp', 'glob_a_0'),
					parentWeapon: this
				});
				//add to the scene, yo
				parentShip.parentState.add(misItem.data);
				if ((getTimer() - lastFired) < cooldown) {
					trace ('firing at ' + getTimer() + ' and before it was ' + lastFired + 'so the cooldown was ' + (getTimer() - lastFired));
				}
				lastFired = getTimer();
			}
		}
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		override protected function initSound():void
		{
			/*
			sequence = new Sequence();
			sequence.initSequence(Pattern.pattern_all_on, 'expl_a_2', 16);
			sequence.parentObject = this;
			*/
			//_ce.registry.sequencer.seq.push(sequence);
		}
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		override public function update(timeDelta:Number):void
		{
			//cooldownCounter += timeDelta*1000; //ms per frame
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */

		
		
		/**
		 * Implement game logic (cooldown, ammo, generator power, etc) here
		 */
		override public function canFire():Boolean {
			//trace (cooldown + ' --- ' + cooldownCounter);
			return ((getTimer() - lastFired) > cooldown) && (parentShip.x > 100 && parentShip.x < 750);
		}
		
		
	}
	
}



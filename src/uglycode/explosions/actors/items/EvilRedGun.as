package uglycode.explosions.actors.items
{
	import flash.utils.getTimer;
	
	import citrus.datastructures.DoublyLinkedListNode;
	
	import nape.geom.Vec2;
	
	import uglycode.explosions.actors.Enemy;
	import uglycode.explosions.game.JetsGunsState;

	public class EvilRedGun extends Weapon
	{
		public var parentShip:Enemy;
		
		public function EvilRedGun(name:String, params:Object = null)
		{
			super(name, params);
			cooldown = 1200;
			damage = 4;
		}
		
		override public function fire(ignored:uint = 0):void {
			//_ce.sound.playSound("gun", 1, 0);
			if (canFire()) {
				var angle:Number = figureOutAngle();
				var misItem:DoublyLinkedListNode = _ce.registry.evilRedShotPool.create({
					x: parentShip.x, // TODO get weapon slot x,y 
					y: parentShip.y+14,  // TODO ...
					speed: Math.floor(250 + (Math.random()*20) + (parentShip.velocity[0]/2)),
					angle:  angle,
					rotation: 0,
					damage: damage,
					fuseDuration: 2000 + (Math.random() * 800),
					view: _ce.registry.assetManager.getMovieFromAtlas('temp', 'temp', 'red_a_0'),
					parentWeapon: this
				});
				//add to the scene, yo
				parentShip.parentState.add(misItem.data);
				if ((getTimer() - lastFired) < cooldown) {
					trace ('firing at ' + getTimer() + ' and before it was ' + lastFired + 'so the cooldown was ' + (getTimer() - lastFired));
				}
				lastFired = getTimer();
				//cooldownCounter = 0;
			}
		}
		
		private function figureOutAngle():Number
		{
			var vec:Vec2 = Vec2.get((_ce.state as JetsGunsState).ship.x - this.parentShip.x, (_ce.state as JetsGunsState).ship.y - this.parentShip.y, true);
			return (vec.angle > 0) ? Math.max(Math.PI - 0.25, vec.angle) : Math.min(-Math.PI + 0.25, vec.angle);
		}		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		override protected function initSound():void
		{
			/*
			sequence = new Sequence();
			sequence.initSequence(Pattern.pattern_all_on, 'expl_a_2', 16);
			sequence.parentObject = this;
			*/
			//_ce.registry.sequencer.seq.push(sequence);
		}
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		override public function update(timeDelta:Number):void
		{
			//cooldownCounter += timeDelta*1000; //ms per frame
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		/**
		 * Implement game logic (cooldown, ammo, generator power, etc) here
		 */
		override public function canFire():Boolean {
			//trace (cooldown + ' --- ' + cooldownCounter);
			return ((getTimer() - lastFired) > cooldown) && (parentShip.x > 100 && parentShip.x < 750);
		}
	}
}
package uglycode.explosions.actors.items
{
	import citrus.datastructures.DoublyLinkedListNode;
	import citrus.view.starlingview.AnimationSequence;
	
	import starling.display.MovieClip;
	
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.sound.Pattern;
	import uglycode.explosions.sound.Sequence;
	
	public class FireballGun extends Weapon
	{
		public var parentShip:Ship;

		
		public function FireballGun(name:String, params:Object = null)
		{
			super(name, params);
		}
		

		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		override protected function initSound():void
		{
			sequence = new Sequence();
			sequence.initSequence(Pattern.pattern_fourth_0, 'bolt', 16);
			sequence.parentObject = this;
			//_ce.registry.sequencer.seq.push(sequence);
		}
		/**
		 * =================================================================== public methods =================================================================
		 */
		override public function update(timeDelta:Number):void
		{
		}
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		override public function fire(ignored:uint = 1):void {
			//var heroAnim:AnimationSequence = new AnimationSequence(Assets.getTextureAtlas("Hero"), ["fly", "descent", "stop", "ascent", "throughPortal", "jump", "ground"], "fly", 30, true);
			//var anim:MovieClip = _ce.registry.assetManager.getMovieFromAtlas('temp','temp','bolt_a_');
			//StarlingArt.setLoopAnimations(["fly"]);
			//_ce.sound.playSound("gun", 1, 0);
			if (sequence.play(_ce.registry.sequencer.currentBeat)) {
				var misItem:DoublyLinkedListNode = _ce.registry.fireballGunShotPool.create({
					x: parentShip.x+33, // TODO get weapon slot x,y 
					y: parentShip.y+8,  // TODO ...
					speed: 165 + (Math.random()*20) + (parentShip.velocity[0]/2),
					angle: (Math.PI*2)-0.05 + (Math.random()*0.08),
					rotation: 0,
					fuseDuration: 1500 + (Math.random() * 600),
					//view: _ce.registry.assetManager.getImageFromAtlas('temp', 'temp', 'bolt_a_01')
					view: _ce.registry.assetManager.getMovieFromAtlas('temp','temp','bolt_a_'),
					parentWeapon: this
				});
				//add to the scene, yo
				parentShip.parentState.add(misItem.data);
			}
		}
		
	}
	
}


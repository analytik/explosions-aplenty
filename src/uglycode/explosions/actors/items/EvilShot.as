package uglycode.explosions.actors.items
{
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	
	import uglycode.explosions.actors.Enemy;
	import uglycode.explosions.game.PhysicsCollisionCategories;

	public class EvilShot extends Shot
	{
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function EvilShot(name:String, params:Object=null)
		{
			super(name, params);
		}
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		/**
		 * =================================================================== public methods =================================================================
		 */
		/**
		 * =================================================================== init ===========================================================================
		 */
		/**
		 * Don't collide with own shots and own missiles
		 */
		override protected function createFilter():void {
			_body.setShapeFilters(new InteractionFilter(PhysicsCollisionCategories.ENEMYSHOT, 
				~(PhysicsCollisionCategories.SHOT | PhysicsCollisionCategories.ENEMYMISSILE | PhysicsCollisionCategories.ENEMY | PhysicsCollisionCategories.ENEMYSHOT | PhysicsCollisionCategories.BOSS)));
		}

		override protected function createConstraint():void {
			_body.space = _nape.space;			
			_body.cbTypes.add(PHYSICS_OBJECT);
			
			// TODO also don't collide with BossEnemy
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, ENEMYSHOT, CbType.ANY_BODY.excluding([SHOT, ENEMYSHOT, Enemy.ENEMY]), handleBeginContact);
			_body.cbTypes.add(ENEMYSHOT);
			_body.space.listeners.add(_beginContactListener);
		}
	}
}
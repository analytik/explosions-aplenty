package uglycode.explosions.actors.items
{
	import nape.callbacks.InteractionCallback;

	public class EvilPlasmaShot extends EvilShot
	{
		public function EvilPlasmaShot(name:String, params:Object=null)
		{
			radius = 8;
			super(name, params);
		}
		

		
		/**
		 * =================================================================== handlers ===========================================================================
		 */
		override public function handleBeginContact(callback:InteractionCallback):void
		{
			if (callback.int1.userData.myData === this) {
				callback.int2.userData.myData.takeDamage(damage);
				violentDeath = true;
				explode();
			}
		}
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
	}
}
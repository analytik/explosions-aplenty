package uglycode.explosions.actors.items
{
	
	import flash.display.MovieClip;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyList;
	import nape.shape.Circle;
	
	import org.osflash.signals.Signal;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.Enemy;
	import uglycode.explosions.game.PhysicsCollisionCategories;

	/**
	 * A missile is an object that moves at a particular trajectory and speed, and explodes when it comes into contact with something.
	 * Often you will want the object that it exploded on to also die (or at least get hurt), such as a hero or an enemy.
	 * Since the missile can potentially be used for any purpose, by default the missiles do not do any damage or kill the object that
	 * they collide with. You will have to handle this manually using the onExplode() handler.
	 * 
	 * Properties:
	 * angle - In degrees, the angle that the missile will fire at. Right is zero degrees, going clockwise.
	 * speed - The speed that the missile moves at.
	 * fuseDuration - In milliseconds, how long the missile lasts before it explodes if it doesn't touch anything.
	 * explodeDuration - In milliseconds, how long the explode animation lasts before the missile object is destroyed.
	 * 
	 * Events
	 * onExplode - Dispatched when the missile explodes. Passes two parameters:
	 * 		1. The Missile (Missile)
	 * 		2. The Object it exploded on (PhysicsObject)
	 */
	public class Missile extends NapePhysicsObject implements IDestructible 
	{
		public static const MISSILE:CbType = new CbType();
		/**
		 * The speed that the missile moves at.
		 */
		public var speed:Number = 200;
		/**
		 * In degrees, the angle that the missile will fire at. Right is zero degrees, going clockwise.
		 */
		public var angle:Number = 0;
		/**
		 * In milliseconds, how long the explode animation lasts before the missile object is destroyed.
		 */
		public var explodeDuration:Number = 1000;
		/**
		 * In milliseconds, how long the missile lasts before it explodes if it doesn't touch anything.
		 */
		public var fuseDuration:Number = 1500;
		/**
		 * Flag to determine whether explosion exerts outward force on nearby dynamic objects
		 */
		public var useForce:Boolean = true;
		/**
		 * if useForce=true, how strong should the displacement explosion be?
		 */
		public var blastRadius:Number = 60;
		/**
		 * Dispatched when the missile explodes. Passes two parameters:
		 * 		1. The Missile (Missile)
		 * 		2. The Object it exploded on (PhysicsObject)
		 */
		public var onExplode:Signal;
		protected var violentDeath:Boolean = false;
		private var _velocity:Vec2;
		private var _exploded:Boolean = false;
		private var _explodeTimeoutID:Number = 0;
		private var _fuseDurationTimeoutID:Number = 0;
		private var _contact:NapePhysicsObject;
		private var _beginContactListener:InteractionListener;
		
		public var level:uint = 1;
		public var damage:int = 15;
		public var hitPoints:Number = 6;
		public var defaultHitPoints:Number = 6;

		
		public static function Make(name:String, x:Number, y:Number, width:Number, height:Number, angle:Number, view:* = null, 
									speed:Number = 200, fuseDuration:Number = 3500, explodeDuration:Number = 1000, 
									blastRadius = 60, useForce:Boolean = true):Missile
		{
			if (view == null) view = MovieClip;
			return new Missile(name, { x: x, y: y, width: width, height: height, angle: angle, view: view,
				speed: speed, fuseDuration: fuseDuration, explodeDuration: explodeDuration, 
				blastRadius: blastRadius, useForce:useForce } );
		}
		
		public function Missile(name:String, params:Object = null)
		{
			super(name, params);
			onExplode = new Signal(Missile, NapePhysicsObject);
		}
		
		override public function initialize(poolObjectParams:Object = null):void {
			
			super.initialize(poolObjectParams);
			
			_velocity = new Vec2(speed, 0);
			_velocity.rotate(angle);
			_inverted = speed < 0;
			_exploded = false;
			kill = false;
			
			_fuseDurationTimeoutID = setTimeout(explode, fuseDuration);
			_body.velocity = _velocity;
		}
		
		override public function destroy():void
		{
			onExplode.removeAll();
			//_fixture.removeEventListener(ContactEvent.BEGIN_CONTACT, handleBeginContact);
			clearTimeout(_explodeTimeoutID);
			clearTimeout(_fuseDurationTimeoutID);
			
			super.destroy();
		}
		
		override public function get rotation():Number
		{
			return angle;
		}
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			updateAnimation();
		}
		
		/**
		 * Explodes the missile
		 */
		public function explode():void
		{
			if (_exploded)
				return;
			
			_exploded = true;
			clearTimeout(_fuseDurationTimeoutID);
			killMissile();
			return;
			//Not collideable with anything anymore.
			// FIXME need a nape alt for this command
			//_fixture.SetFilterData({ maskBits: Box2DCollisionCategories.GetNone() });
			
			onExplode.dispatch(this, _contact);
			
			_explodeTimeoutID = setTimeout(killMissile, explodeDuration);
			
			
			// here we jump into the body list of the nape space, and poll for distance from bomb. If close enough push force from explosion point
			// TODO rip this code to get missile navigation
			if (useForce) {
				
				var explosionVec2:Vec2 = new Vec2(x, y);
				
				var bodies:BodyList = _nape.space.bodies;
				var b:Body;
				var ballVec2:Vec2;
				var impulseVector:Vec2;
				var ll:uint = bodies.length;
				for (var i:int = 0; i < ll; i ++) {
					b = bodies.at(i);
					if (!b.isDynamic()) continue;
					ballVec2 = b.position;
					impulseVector = new Vec2(ballVec2.x - explosionVec2.x, ballVec2.y - explosionVec2.y);
					trace (impulseVector.toString());
					if (impulseVector.length < blastRadius) {
						var impulseForce:Number = (blastRadius - impulseVector.length) / 30;
						var impulse:Vec2 = new Vec2(impulseVector.x * impulseForce, impulseVector.y * impulseForce * 1.4);
						//trace (impulse.toString());
						b.applyImpulse(impulse);
					}
				}
			}
		}
		
		
		
		
		override protected function defineBody():void
		{
			super.defineBody();
		}
		

		
		override protected function createBody():void 
		{
			super.createBody();
			_body.gravMass = 0;
			_body.allowRotation = false;
		}
		
		
		
		/**
		 * This method will often need to be overriden to customize the Nape shape object.
		 * The PhysicsObject creates a rectangle by default if the radius it not defined, but you can replace this method's
		 * definition and instead create a custom shape, such as a line or circle.
		 */	
		override protected function createShape():void {
			_shape = new Circle(4.5, null, _material);
			_body.shapes.add(_shape);
		}
		
		
		/**
		 * Don't collide with own shots and own missiles
		 */
		override protected function createFilter():void {
			_body.setShapeFilters(new InteractionFilter(PhysicsCollisionCategories.SHOT, 
				~(PhysicsCollisionCategories.SHOT | PhysicsCollisionCategories.MISSILE | PhysicsCollisionCategories.SHIP | PhysicsCollisionCategories.ENEMYSHOT)));
		}

		
		
		override protected function createConstraint():void {
			
			_body.space = _nape.space;
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, MISSILE, CbType.ANY_BODY.excluding([Shot.SHOT, MISSILE]), handleBeginContact);
			//_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, SHOT, CbType.ANY_BODY.excluding(SHOT), handleEndContact);
			_body.cbTypes.add(Shot.SHOT);
			_body.space.listeners.add(_beginContactListener);
			_body.cbTypes.add(MISSILE);
		}
		
		
		
		override public function handleBeginContact(callback:InteractionCallback):void
		{
			if (callback.int1.userData.myData === this) {
				if (callback.int2.userData.myData is EvilShot) {
					//trace ('taking damage ' + callback.int2.userData.myData.damage);
					takeDamage(callback.int2.userData.myData.damage);
				}
				else {
					takeDamage(200);
				}
			}
		}
		
		
		public function takeDamage(dmg:Number):void
		{
			// cycki
			// if shield...
			hitPoints -= dmg;
			checkHealth();
		}
		
		// TODO rework as damage setter? too magical? or Event onChange?
		protected function checkHealth():void
		{
			// TODO Auto Generated method stub
			if (hitPoints <= 0) {
				//(_ce as Explosions).registry.stats.ePenisLength += score;
				this.playDeathSound();
				violentDeath = true;
				explode();
				hitPoints = defaultHitPoints;
			}
		}
		
		
		protected function playDeathSound():void
		{
			_ce.sound.playSound(getDeathSound(), 1, 1);
		}
		
		private function getDeathSound():String {
			return 'missile_b';
		}

		
		protected function updateAnimation():void
		{
			if (_exploded)
			{
				_animation = "exploded";
			}
			else
			{
				_animation = "normal";
			}
		}
		
		protected function killMissile():void
		{
			kill = true;
		}
	}

}
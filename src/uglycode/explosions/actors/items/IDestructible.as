package uglycode.explosions.actors.items
{
	import nape.callbacks.InteractionCallback;

	public interface IDestructible
	{
		/**
		 * Damage/Hit Points are floats, because of damage % bonuses / resistances 
		 */
		function takeDamage(dmg:Number):void;
		
		function handleBeginContact(callback:InteractionCallback):void;
		
	}
}
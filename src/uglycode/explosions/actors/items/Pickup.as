package uglycode.explosions.actors.items
{
	import citrus.objects.NapePhysicsObject;
	
	import flash.utils.clearTimeout;
	import flash.utils.getQualifiedClassName;
	import flash.utils.setTimeout;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	
	import org.osflash.signals.Signal;
	
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.game.PhysicsCollisionCategories;

	/**
	 * @description This is a superclass for all cash bonuses / powerups / booby traps
	 */
	public class Pickup extends NapePhysicsObject
	{
		public static const MONEY:CbType = new CbType();
		public static const POWERUP:CbType = new CbType();
		public static const TRAP:CbType = new CbType();
		public static const PICKUP:CbType = new CbType();
		
		
		/**
		 * The speed that the missile moves at.
		 */
		public var speed:Number = 20;
		/**
		 * In milliseconds, how long the gunshot lasts before it disappears if it doesn't touch anything.
		 */
		public var fuseDuration:Number = 4500;
		/**
		 * Dispatched when the missile explodes. Passes two parameters:
		 * 		1. The Missile (Missile)
		 * 		2. The Object it exploded on (PhysicsObject)
		 */
		public var onExpire:Signal;
		
		protected var _beginContactListener:InteractionListener;
		protected var _velocity:Vec2;
		protected var _expired:Boolean = false;
		protected var _fuseDurationTimeoutID:Number = 0;
		public var level:uint = 1;
		public var damage:int = 10;
		/********************************************* MORON DON"T FORGET TO RE-initialize() ALL VARIABLES THAT YOU ADD HERE *******************************/

		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Pickup(name:String, params:Object = null)
		{
			super(name, params);
			//onTakeDamage = new Signal();
			
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		

		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			_body.velocity = _velocity;
		}
		
		
		
		override public function destroy():void
		{
			//onExplode.removeAll();
			clearTimeout(_fuseDurationTimeoutID);
			
			super.destroy();
		}
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		override public function initialize(poolObjectParams:Object = null):void {
			super.initialize(poolObjectParams);
			view.alpha = 0.9;
			
			_fuseDurationTimeoutID = setTimeout(expire, fuseDuration);
			_body.velocity = _velocity;
		}


		override protected function defineBody():void {
			_bodyType = BodyType.STATIC;
		}
		
		override protected function createBody():void 
		{
			super.createBody();
			_body.gravMass = 0;
			_body.allowRotation = false;
		}
		
		override protected function createFilter():void {
			
			_body.setShapeFilters(new InteractionFilter(0, 0 , 1, PhysicsCollisionCategories.SHIP, 0, 0));
		}
		
		override protected function createConstraint():void {
			// TODO inspect if those 2 lines are needed
			//_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.SENSOR, PICKUP, Ship.SHIP, handleBeginContact);
			//_body.space.listeners.add(_beginContactListener);
			_body.space = _nape.space;			
			_body.cbTypes.add(PICKUP);
		}
		

		
		
		
		
		/**
		 * =================================================================== handlers ===========================================================================
		 */
		override public function handleBeginContact(callback:InteractionCallback):void
		{
			//trace ('shot type: ' + getQualifiedClassName(callback.int1.userData.myData) + ' hit the objects' + getQualifiedClassName(callback.int2.userData.myData));
			if (callback.int1.userData.myData === this) {
				// TODO do something to int2 (add the bonus or somesuch)
				expire();
			}
		}
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */


		
		/**
		 * Remove the projectile
		 */
		public function expire():void
		{
			if (_expired)
				return;
			
			_expired = true;
			_body.space.listeners.remove(_beginContactListener);
			_beginContactListener = null;
			clearTimeout(_fuseDurationTimeoutID);
			kill = true;
			
			// TODO add splinters and shit?
			// TODO add debris class which will be a subclass of particle system?
		}
		
		
		
	
	
	
	
	
	
	
	}
}
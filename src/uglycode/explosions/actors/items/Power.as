package uglycode.explosions.actors.items
{
	import org.osflash.signals.Signal;
	
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.game.PowerManager;
	import uglycode.explosions.game.Registry;
	import uglycode.explosions.sound.ISequenceTimed;

	public class Power implements ISequenceTimed
	{
		/**
		 * Cooldown in beats, if 0, power is ready
		 */
		public var cooldown:uint;
		public var currentCooldown:uint = 0;
		
		/**
		 * Generator resources needed (TeraWatt)
		 */
		public var energyNeeded:uint;
		
		/**
		 * Status: ready | cooldown | disabled
		 */
		public var status:String;
		
		/**
		 * If damaged by EMP, time to repair [beats], if 0, power is ready
		 */
		public var repairCooldown:uint;
		public var currentRepairCooldown:uint;
		
		/**
		 * sound to play on firing
		 */
		public var fireSound:String;
		
		/**
		 * Sprite data
		 */
		public var viewAtlas:String;
		public var viewGroup:String;
		public var viewSprite:String;
		/**
		 * where to put the power icon?
		 */
		public var slot:String = PowerManager.SLOT_WEAPONS;
		public var name:String;
		
		public var parentShip:Ship;
		public var registry:Registry;
		public var changedFlag:Boolean;
		public var onChange:Signal;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		public function Power()
		{
			//this signal is used only to update UI
			onChange = new Signal(String, String, uint); //slotName, powerName, cooldown
		}
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		public function get totalCooldown():uint {
			return currentCooldown + currentRepairCooldown;
		}
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		public function update():void {
			changedFlag = false;
			if (currentRepairCooldown > 0) {
				changedFlag = true;
				currentRepairCooldown--;
				status = 'disabled';
			}
			else if (currentCooldown > 0) {
				changedFlag = true;
				currentCooldown--;
				status = 'cooldown';
			}
			if (status != 'ready' && currentCooldown == 0 && currentRepairCooldown == 0) {
				changedFlag = true;
				status = 'ready';
			}
			if (changedFlag) {
				onChange.dispatch(this.slot, this.name, this.totalCooldown);
			}
		}
		
		
		
		public function destroy():void {
			onChange.removeAll();
		}
		
		
		
		/**
		 * =================================================================== handlers ===========================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		public function fire():void {
			throw new Error('Override me, fuckass.');
		}
		
		
	}
}
package uglycode.explosions.actors.items
{
	import citrus.datastructures.DoublyLinkedListNode;
	
	import uglycode.explosions.actors.Ship;
	import uglycode.explosions.sound.Pattern;
	import uglycode.explosions.sound.Sequence;
	

	
	public class MachineGun extends Weapon
	{
		public var parentShip:Ship;
		
		public function MachineGun(name:String, params:Object = null)
		{
			super(name, params);
		}
		
		override public function fire(ignored:uint = 1):void {
			//_ce.sound.playSound("gun", 1, 0);
			if (sequence.play(_ce.registry.sequencer.currentBeat)) {
				var misItem:DoublyLinkedListNode = _ce.registry.machineGunShotPool.create({
					x: parentShip.x+33, // TODO get weapon slot x,y 
					y: parentShip.y+8,  // TODO ...
					speed: 180 + (Math.random()*20) + (parentShip.velocity[0]/2),
					angle:  (Math.random()*0.03) - 0.015,
					rotation: 0,
					fuseDuration: 2000 + (Math.random() * 800),
					view: _ce.registry.assetManager.getMovieFromAtlas('temp', 'temp', 'gun_a_0'),
					parentWeapon: this
				});
				//add to the scene, yo
				parentShip.parentState.add(misItem.data);
			}
		}

		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		

		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		override protected function initSound():void
		{
			sequence = new Sequence();
			sequence.initSequence([0,1,1,0, 0,0,1,0, 0,1,1,0, 0,0,1,0], 'expl_b_2', 16);
			sequence.parentObject = this;
			//_ce.registry.sequencer.seq.push(sequence);
		}

		/**
		 * =================================================================== public methods =================================================================
		 */
		override public function update(timeDelta:Number):void
		{
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
	}
	
}
package uglycode.explosions.actors.items
{
	import flash.utils.clearTimeout;
	import flash.utils.getQualifiedClassName;
	import flash.utils.setTimeout;
	
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.geom.Vec2List;
	import nape.shape.Polygon;
	
	import org.osflash.signals.Signal;
	
	import uglycode.explosions.game.PhysicsCollisionCategories;

	/**
	 * @description This is a superclass for all projectiles/missiles/laz0rs/etc
	 */
	public class Shot extends NapePhysicsObject
	{
		public static const SHOT:CbType = new CbType();
		public static const ENEMYSHOT:CbType = new CbType();
		
		public var parentWeapon:Weapon;
		
		/**
		 * The speed that the missile moves at.
		 */
		public var speed:Number = 200;
		/**
		 * In degrees, the angle that the missile will fire at. Right is zero degrees, going clockwise.
		 */
		public var angle:Number = 0;
		/**
		 * In milliseconds, how long the gunshot lasts before it disappears if it doesn't touch anything.
		 */
		public var fuseDuration:Number = 1500;
		/**
		 * Dispatched when the missile explodes. Passes two parameters:
		 * 		1. The Missile (Missile)
		 * 		2. The Object it exploded on (PhysicsObject)
		 */
		public var onExplode:Signal;
		
		protected var _beginContactListener:InteractionListener;
		protected var _exploded:Boolean = false;
		protected var _fuseDurationTimeoutID:Number = 0;
		protected var _contact:NapePhysicsObject;
		public var level:uint = 1;
		public var damage:int = 10;
		protected var violentDeath:Boolean = false;
		/********************************************* MORON DON"T FORGET TO RE-initialize() ALL VARIABLES THAT YOU ADD HERE *******************************/

		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Shot(name:String, params:Object = null)
		{
			super(name, params);
			//onTakeDamage = new Signal();
			
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		override public function get rotation():Number
		{
			return angle;
		}
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			
			updateAnimation();
		}
		
		
		
		override public function destroy():void
		{
			//onExplode.removeAll();
			clearTimeout(_fuseDurationTimeoutID);
			
			super.destroy();
		}
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		override public function initialize(poolObjectParams:Object = null):void {
			kill = false;
			super.initialize(poolObjectParams);
			view.alpha = this.parentWeapon.getTransparencyLevel();
			var tempVelocity:Vec2 = Vec2.get(speed, 0);
			tempVelocity.rotate(angle);
			_inverted = speed < 0;
			_exploded = false;
			violentDeath = false;
			//kill = false;
			
			_fuseDurationTimeoutID = setTimeout(explode, fuseDuration);
			velocity = [ tempVelocity.x, tempVelocity.y ];
		}
		/*
		// only override this in child classes
		override protected function createConstraint():void {
			super.createConstraint();
			_body.shapes.clear();
			_body.shapes.add(new Polygon( [   Vec2.weak(6,34)   ,  Vec2.weak(28,29)   ,  Vec2.weak(40,16)   ,  Vec2.weak(6,10)   ] ));
			_body.shapes.add(new Polygon( [   Vec2.weak(28,29)   ,  Vec2.weak(44,29)   ,  Vec2.weak(44,20)   ,  Vec2.weak(40,16) ] ));
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, HERO, CbType.ANY_BODY, handleBeginContact);
			_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, HERO, CbType.ANY_BODY, handleEndContact);
		}
		*/

		override protected function defineBody():void
		{
			super.defineBody();
		}
		
		
		
		override protected function createBody():void 
		{
			super.createBody();
			_body.gravMass = 0;
			_body.allowRotation = false;
		}
		
		/**
		 * Don't collide with own shots and own missiles 
		 */
		override protected function createFilter():void {
			_body.setShapeFilters(new InteractionFilter(PhysicsCollisionCategories.SHOT, 
				~(PhysicsCollisionCategories.SHOT | PhysicsCollisionCategories.MISSILE | PhysicsCollisionCategories.SHIP | PhysicsCollisionCategories.ENEMYSHOT)));
		}
		
		
		/**
		 * Set up CbTypes and such here.
		 */
		override protected function createConstraint():void {
			super.createConstraint();
			
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, SHOT, CbType.ANY_BODY.excluding([SHOT, Missile.MISSILE, ENEMYSHOT]), handleBeginContact);
			//_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, SHOT, CbType.ANY_BODY.excluding(SHOT), handleEndContact);
			_body.cbTypes.add(SHOT);
			_body.space.listeners.add(_beginContactListener);
			//_body.space.listeners.add(_endContactListener);
		}
		
		
		
		/**
		 * =================================================================== handlers ===========================================================================
		 */
		override public function handleBeginContact(callback:InteractionCallback):void
		{
			//trace ('shot type: ' + getQualifiedClassName(callback.int1.userData.myData) + ' hit the objects' + getQualifiedClassName(callback.int2.userData.myData));
			if (callback.int1.userData.myData === this) {
				violentDeath = true;
				explode();
			}
		}
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */


		
		/**
		 * Remove the projectile
		 */
		public function explode():void
		{
			if (_exploded)
				return;
			/*
			if (violentDeath) {
				trace ('Killing one shot ' + this.name);
			}
			else {
				trace ('One shot has died of old age.');
			}*/
			
			_exploded = true;
			
			//onExplode.dispatch(this, _contact);
			_body.space.listeners.remove(_beginContactListener);
			_beginContactListener = null;
			
			clearTimeout(_fuseDurationTimeoutID);
			kill = true;
			
			// TODO add splinters and shit?
			// TODO add debris class which will be a subclass of particle system?
		}
		
		
		
		protected function updateAnimation():void
		{
			if (_exploded)
			{
				_animation = "exploded";
			}
			else
			{
				_animation = "normal";
			}
		}
	
		/*protected function getParentWeapon():Weapon {
			throw new Error('Override this method, fuckass');
			return new Weapon('well, fvck');
		}*/
	
	
	
	
	
	
	
	
	
	}
}
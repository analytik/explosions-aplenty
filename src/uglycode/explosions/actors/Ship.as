package uglycode.explosions.actors
{
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	import flash.utils.getDefinitionByName;
	
	import citrus.core.StarlingState;
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.shape.Polygon;
	
	import org.osflash.signals.Signal;
	
	import starling.display.Image;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.items.IDestructible;
	import uglycode.explosions.actors.items.MissilePower;
	import uglycode.explosions.game.JetsGunsState;
	import uglycode.explosions.game.PhysicsCollisionCategories;
	import uglycode.explosions.game.PowerManager;
	import uglycode.explosions.sound.Sequencer;

	/**
	 * Player's ship
	 * TODO: Superclass this, so we can have different ships with different properties or game types altogether
	 */
	public class Ship extends NapePhysicsObject implements IDestructible
	{
		public static const MOVEMENT_BOUNDARY_LEFT:int = 30;
		public static const MOVEMENT_BOUNDARY_TOP:int = 30;
		public static const MOVEMENT_BOUNDARY_RIGHT:int = 330;
		public static const MOVEMENT_BOUNDARY_BOTTOM:int = 400;
		
		//properties
		/**
		 * This is the rate at which the hero speeds up when you move him left and right. 
		 */
		[Inspectable(defaultValue="30")]
		public var acceleration:Number = 30;
		
		/**
		 * This is the fastest speed that the hero can move left or right. 
		 */
		[Inspectable(defaultValue="120")]
		public var maxVelocity:Number = 150;
		
		/**
		 * This is the amount of "float" that the hero has when the player holds the jump button while jumping. 
		 */
		[Inspectable(defaultValue="3")]
		public var jumpAcceleration:Number = 3;

		
		public var sequencer:Sequencer;
		public var weapons:Array;
		public var powerManager:PowerManager;
		public var parentState:JetsGunsState;

		
		/**
		 * =================================================================== SIGNALS ========================================================================
		 * Dispatched whenever the hero takes damage from an enemy. 
		 */
		public var onTakeDamage:Signal;
		
		/**
		 * Dispatched whenever the hero's animation changes. 
		 */
		public var onAnimationChange:Signal; //todo remove
		
		[Deprecated]
		protected var _groundContacts:Array = [];// Used to determine if he's on ground or not. //todo remove
		[Deprecated]
		protected var _enemyClass:Class = Enemy; //todo inspect
		[Deprecated]
		protected var _onGround:Boolean = false; //todo remove
		[Deprecated]
		protected var _springOffEnemy:Number = -1; //todo remove
		[Deprecated]
		protected var _hurtTimeoutID:Number; //todo remove or reuse
		[Deprecated]
		protected var _hurt:Boolean = false; //todo remove or reuse
		protected var _friction:Number = 50.75; //todo calibrate
		
		protected var _playerMovingHero:Boolean = false; //todo add more/less jet flames/particles
		protected var _controlsEnabled:Boolean = true; // todo use
		[Deprecated]
		protected var _combinedGroundAngle:Number = 0; // todo inspect and remove
		protected var sView:Image;
		
		// interaction listeners
		private var _beginContactListener:InteractionListener; //todo remove and learn how to use
		private var _endContactListener:InteractionListener;
		
		public static const SHIP:CbType = new CbType();
		private var sign:Number;
		private var maxFriction:Number;
		//private var _velocity:Vec2;
		public var defaultHitPoints:Number = 5;
		public var defaultShield:Number = 5;
		
		public var hitPoints:Number;
		public var shield:Number;
		public var dead:Boolean;
		private var lastShieldRecharge:Object;

		
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function Ship(name:String, params:Object = null) {
			
			super(name);
			// wait no //a bit of duplicate work, eh, whatevs
			initialize(params);
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		override public function set x(value:Number):void
		{
			_x = Math.max(Math.min(value, 330), 5);
			//trace ('overriding ship X for some reason?! - ' + _x);
			if (_body)
			{
				var pos:Vec2 = _body.position;
				pos.x = _x;
				_body.position = pos;
			}
		}
		
		
		
		override public function set y(value:Number):void {
			_y = value;
			
			if (_body)
			{
				var pos:Vec2 = _body.position;
				pos.y = _y;
				_body.position = pos;
			}
		}
		
		
		/**
		 * Whether or not the player can move and jump with the hero. 
		 */
		public function get controlsEnabled():Boolean {
			return _controlsEnabled;
		}
		
		public function set controlsEnabled(value:Boolean):void {
			_controlsEnabled = value;
			// if (!_controlsEnabled)
			// _fixture.SetFriction(_friction);
		}
		
		/**
		 * Returns true if the hero is on the ground and can jump. 
		 */
		/*
		public function get onGround():Boolean {
			return _onGround;
		}
		*/
		
		/**
		 * The Hero uses the enemyClass parameter to know who he can kill (and who can kill him).
		 * Use this setter to to pass in which base class the hero's enemy should be, in String form
		 * or Object notation.
		 * For example, if you want to set the "Enemy" class as your hero's enemy, pass
		 * "com.citrusengine.objects.platformer.Enemy", or Enemy (with no quotes). Only String
		 * form will work when creating objects via a level editor.
		 */
		/*
		[Inspectable(defaultValue="uglycode.explosions.actors.Enemy",type="String")]
		public function set enemyClass(value:*):void {
			
			if (value is String)
				_enemyClass = getDefinitionByName(value as String) as Class;
			else if (value is Class)
				_enemyClass = value;
		}*/
		
		public function getControlRectangle():Rectangle {
			return new Rectangle(Ship.MOVEMENT_BOUNDARY_LEFT, Ship.MOVEMENT_BOUNDARY_TOP, MOVEMENT_BOUNDARY_RIGHT-MOVEMENT_BOUNDARY_LEFT, MOVEMENT_BOUNDARY_BOTTOM-MOVEMENT_BOUNDARY_TOP);
		}

		/**
		 * =================================================================== public methods =================================================================
		 */
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			var velocityX:Number = velocity[0];
			var velocityY:Number = velocity[1];
			//sView.scaleX = 1;
			
			
			if (controlsEnabled)
			{
				var moveKeyPressed:Boolean = false;
				
				//this ugly code is ugly, it's here only because I copied it from another class. Shut up, I'll refactor it later.
				if (_ce.input.isDoing('right') && x < 330)
				{
					_ce.input.getActionsSnapshot()
					velocityX += acceleration;
					moveKeyPressed = true;
				}
				
				if (_ce.input.isDoing('left') && x > 30)
				{
					velocityX -= acceleration;
					moveKeyPressed = true;
				}
				if (_ce.input.isDoing('up') && y > 30)
				{
					velocityY -= acceleration;
					moveKeyPressed = true;
				}
				
				if (_ce.input.isDoing('down') && y < 400)
				{
					velocityY += acceleration;
					moveKeyPressed = true;
				}
				
				//If player just started moving the hero this tick.
				if (moveKeyPressed && !_playerMovingHero)
				{
					_playerMovingHero = true;
					//_fixture.SetFriction(0); //Take away friction so he can accelerate.
				}
					//Player just stopped moving the hero this tick.
				else if (!moveKeyPressed)
				{
					_playerMovingHero = false;
					//_fixture.SetFriction(_friction); //Add friction so that he stops running
					velocityX = (Math.abs(velocityX) > 0.001) ? velocityX - Math.min(_friction, Math.abs(velocityX)) * (velocityX / Math.abs(velocityX)) : 0;
					velocityY = (Math.abs(velocityY) > 0.001) ? velocityY - Math.min(_friction, Math.abs(velocityY)) * (velocityY / Math.abs(velocityY)) : 0;
				}
				
				// TODO clumsy solution, come up with sth more elegant?
				// TODO replace this awful mess with 4 static shapes with a separate collision filter, colliding only with Ship collision group, DUH SO EASY 
				if (y + (velocityY/50) > 400) {
					if (y > 400 || velocityY > 0.001) {
						velocityY = Math.min((400 - y) * 3, -0.7);
					}
				}
				else if (y + (velocityY/50) < 30) {
					if (y < 30 || velocityY < -0.001) {
						velocityY = Math.max((30 - y) * 3, 0.7);
					}
				}
				if (x + (velocityX/50) > 330) {
					if (x > 330 || velocityX > 0.001) {
						velocityX = Math.min((330 - x) * 3, -0.7);
					}
				}
				else if (x + (velocityX/50) < 30) {
					if (x < 30 || velocityX < -0.001) {
						velocityX = Math.max((30 - x) * 3, 0.7);
					}
				}
				
				
				
				//Cap velocities
				if (velocityX > (maxVelocity)) {
					velocityX = maxVelocity;
				}
				else if (velocityX < (-maxVelocity)) {
					velocityX = -maxVelocity;
				}
				if (velocityY > (maxVelocity)) {
					velocityY = maxVelocity;
				}
				else if (velocityY < (-maxVelocity)) {
					velocityY = -maxVelocity;
				}
				
				//update physics with new velocity
				//_body.velocity = _velocity;
				velocity = [ velocityX, velocityY ];
				
			}
			updateWeapons(timeDelta);
			//updateAnimation();
			
			
			// update the animation
			var prevAnimation:String = _animation;
			
			/* TODO unfuckup this and add weight
			trace ('before: ' + velocity[1]);
			var removeGravity:Vec2 = new Vec2();
			removeGravity.subeq(_nape.gravity);
			removeGravity.muleq(_body.mass);
			_body.applyImpulse(removeGravity);
			trace (' after: ' + velocity[1]);
			*/

			
			
			if (prevAnimation != _animation)
			{
				onAnimationChange.dispatch();
			}
			//recharge shield every now and then
			if (this.sequencer.currentBeat % 6 == 0 && this.lastShieldRecharge != this.sequencer.currentBeat && shield < defaultShield) {
				shield = Math.min(shield+1, defaultShield);
				this.lastShieldRecharge = this.sequencer.currentBeat;
				checkHealth();
			}
			
		}
		
		
		
		override public function destroy():void {
			//onJump.removeAll();
			//onGiveDamage.removeAll();
			onTakeDamage.removeAll();
			onAnimationChange.removeAll();
			
			if (weapons) {
				for (var i:int = 0; i < weapons.length; i++)
				{
					weapons[i].parentShip = null;
				}
				weapons = null;
			}
			
			if (_beginContactListener) {
				_beginContactListener.space = null;
				_beginContactListener = null;
			}
			
			if (_endContactListener) {
				_endContactListener.space = null;
				_endContactListener = null;
			}
			_body.shapes.clear();
			_body.space = null
			super.destroy();
		}
		
		
		
		override public function handleBeginContact(e:InteractionCallback):void {
			
			// TODO - add damage to both colliding objects. Slow down enemy ship, so it doesn't push so violently 
			
			//trace("------------------------------");
			//trace("begin contact:", e.int2.castBody.userData.myData);
			//trace ("I shouldn't even be here - begin contact?");
			/*
			var body2:Body = e.int2.castBody;
			_groundContacts.push(body2);
			//trace("ground contacts:", _groundContacts.length);
			if (e.arbiters.at(0).collisionArbiter) {
				var angle:Number = e.arbiters.at(0).collisionArbiter.normal.angle * 180 / Math.PI;
				if ((45 < angle) && (angle < 135)) {
					_onGround = true;
				}
			}
			*/
		}
		
		
		
		override public function handleEndContact(e:InteractionCallback):void {
			//trace("****************************");
			//trace("end contact:", e.int2.castBody.userData.myData);
			// TODO what did this do?
			//_groundContacts.splice(_groundContacts.indexOf(e.int2.castBody), 1);
			//trace("ground contacts:", _groundContacts.length);
		}
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		override public function initialize(poolObjectParams:Object=null):void {
			// TODO TACKY defaults - REFACTOR
			if (!poolObjectParams) {
				trace("fuck off I'll init later! I'm on a boatship.");
				return;
			}
			if (poolObjectParams) {
				trace ("ship initing now");
				if (!poolObjectParams.hasOwnProperty('x')) poolObjectParams.x = 140;
				if (!poolObjectParams.hasOwnProperty('y')) poolObjectParams.y = 220;
				if (!poolObjectParams.hasOwnProperty('hitPoints')) poolObjectParams.hitPoints = this.defaultHitPoints;
				if (!poolObjectParams.hasOwnProperty('shield')) poolObjectParams.shield = this.defaultShield;
				if (!poolObjectParams.hasOwnProperty('group')) poolObjectParams.group = 5000;
			}
			this.controlsEnabled = true;
			this.hitPoints = this.defaultHitPoints;
			this.shield = this.defaultShield;
			
			//I'm sorry, but we have to go through this shit again - body, shapes, constraints...
			super.initialize(poolObjectParams);
			//onJump = new Signal();
			//onGiveDamage = new Signal();
			onTakeDamage = new Signal();
			onAnimationChange = new Signal();
			if (_view is Image) {
				// hack for code completion
				sView = _view;
			}
			
			//weapons and powers
			for (var i:int = 0; i < weapons.length; i++)
			{
				weapons[i].parentShip = this;
			}
			this.powerManager.parentShip = this;
			this.powerManager.removeAllPowers();
			this.powerManager.addPower(new MissilePower());

			
			
			/*
			hero.onGiveDamage.add(heroAttack);
			hero.onTakeDamage.add(heroHurt);
			
			private function heroHurt():void {
			_ce.sound.playSound("Hurt", 1, 0);
			}
			*/
			
			
			/*
			smoke = new PDParticleSystem(XML(new Assets.smokeXML()),
			Assets.ta.getTexture("smoke"));
			Starling.juggler.add(smoke);
			play.addChild(smoke);
			smoke.start();
			*/

		}
		
		
		
		override protected function createShape():void {
			_body.shapes.add(new Polygon( [   Vec2.weak(6,34)   ,  Vec2.weak(28,29)   ,  Vec2.weak(40,16)   ,  Vec2.weak(6,10)   ] ));
			_body.shapes.add(new Polygon( [   Vec2.weak(28,29)   ,  Vec2.weak(44,29)   ,  Vec2.weak(44,20)   ,  Vec2.weak(40,16) ] ));
			//_body.localCOM = Vec2.get(0,42);
			//_body.scaleShapes(0.999, 1.001);
			_body.translateShapes(Vec2.weak(-30,-21));
			//_body.align(); //this will reset my translateShapes above
		}
		
		
		
		override protected function createConstraint():void {
			
			_body.space = _nape.space;			
			_body.cbTypes.add(SHIP);
			
			//I think this handles collision detection with ANY_BODY, interesting
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, SHIP, CbType.ANY_BODY, handleBeginContact);
			_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, SHIP, CbType.ANY_BODY, handleEndContact);

			_body.space.listeners.add(_beginContactListener);
			_body.space.listeners.add(_endContactListener);
			
			_body.gravMass = 0;
			//TODO ADD GRAVMASS
			//let's make ship twice as dense so it doesn't get pushed off by light objects 
			//_body.gravMass *= 4;
			//trace('grav mass is ' + _body.gravMass);
			
			//this is last function called in initialize(), so let's create an easily gettable velocity (please don't backfire on me)
			// not needed since CE310
			//_velocity = _body.velocity;
		}

		
		
		override protected function createBody():void {
			
			super.createBody();
			
			_body.allowRotation = false;
		}	

		/**
		 * Don't collide with own shots, missiles and decorative scenery 
		 */
		override protected function createFilter():void {
			_body.setShapeFilters(new InteractionFilter(PhysicsCollisionCategories.SHIP, ~(PhysicsCollisionCategories.SHOT | PhysicsCollisionCategories.MISSILE | PhysicsCollisionCategories.SCENERY)) );
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		private function updateWeapons(timeDelta:Number):void
		{
			for (var i:int = 0; i < weapons.length; i++)
			{
				if (controlsEnabled && _ce.input.isDoing('fire')) //TODO or autofire is on (mobile)
				{
					// TODO add any engine/shield/power/rigs/EMP checks here inside Ship
					weapons[i].fire();
				}
				weapons[i].update(timeDelta);
			}
		}		

		
		
		public function takeDamage(dmg:Number):void {
			if (shield > 0) {
				shield -= dmg;
				trace('taking ' +dmg+' shield damage, remaining shield/HP: ' + shield + '/' + hitPoints);
				if (shield < 0) {
					dmg = -shield;
					shield = 0;
				} else dmg = 0;
			}
			if (dmg > 0) {
				hitPoints -= dmg;
				trace('taking ' +dmg+' health damage, remaining shield/HP: ' + shield + '/' + hitPoints);
			}
			checkHealth();
		}
		
		
		
		// TODO rework as damage setter? too magical? or Event onChange?
		protected function checkHealth():void
		{
			// TODO replace with Signals
			parentState.ui.updateHitPoints(hitPoints);
			parentState.ui.updateShield(shield);
			if (hitPoints <= 0) {
				dead = true;
				parentState.checkGameOverState();
			}
		}		

		

	
	
	
	
	}
}
package uglycode.explosions.actors
{
	import flash.utils.getQualifiedClassName;
	
	import citrus.core.StarlingState;
	import citrus.objects.NapePhysicsObject;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.shape.Polygon;
	
	import org.osflash.signals.Signal;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	
	import uglycode.explosions.Explosions;
	import uglycode.explosions.actors.items.IDestructible;
	import uglycode.explosions.actors.items.Missile;
	import uglycode.explosions.actors.items.Shot;
	import uglycode.explosions.game.PhysicsCollisionCategories;
	import uglycode.explosions.sound.Sequence;
	import uglycode.explosions.world.Spawner;
	
	public class Enemy extends NapePhysicsObject implements IDestructible
	{
		/**
		 * This is the fastest speed that the hero can move left or right. 
		 */
		[Inspectable(defaultValue="120")]
		public var maxVelocity:Number = 120;
		
		public var sequence:Sequence; //enemysequence
		public var weapons:Array = [];
		public var angle:Number;
		public var speed:Number;
		public var parentState:StarlingState;
		public var parentSpawner:Spawner;
		public var amplitude:uint = 220;
		public var topBoundary:uint = 40;
		public var startingY:uint;
		public var bottomBoundary:uint = 390;
		public var level:uint = 1;
		public var hitPoints:Number = 50;
		public var defaultHitPoints:Number = 50;
		public var score:int = 10; //can be negative for shooting down friendlies?
		/**
		 * For tweening, as it's not healthy to force-set X/Y with Nape
		 */
		public var targetX:Number = 0;
		public var targetY:Number = -10;

		
		
		/**
		 * =================================================================== SIGNALS ========================================================================
		 * Dispatched whenever the hero takes damage from an enemy. 
		 */
		public var onTakeDamage:Signal;
		
		protected var sView:Image;
		
		// interaction listeners
		private var _beginContactListener:InteractionListener; //todo remove and learn how to use
		private var _endContactListener:InteractionListener;
		
		public static const ENEMY:CbType = new CbType();
		
		private var sign:Number;
		private var maxFriction:Number;
		private var _velocity:Vec2; //wtf is this shit?
		protected var ownSequence:uint = 0;
		protected var pausedAmplitude:Boolean;
		protected var pauseTimer:uint;
		private var tween:Tween;
		
		
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function Enemy(name:String, params:Object = null) {
			// TODO add real shape
			this.radius = 20;
			super(name, params);
			this.tween = new Tween(this, 4);
			onTakeDamage = new Signal();
			if (_view is Image) {
				sView = _view;
			}
			if (weapons)
			for (var i:int = 0; i < weapons.length; i++) 
			{
				weapons[i].parentShip = this;
			}
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		override public function update(timeDelta:Number):void
		{
			//super.update(timeDelta);
			//sView.scaleX = 1;

			//var newy:Number = Math.max(Math.min(bottomBoundary, startingY + Math.sin(ownSequence++)*amplitude), topBoundary);
			if (pausedAmplitude === false) {
				if (++pauseTimer > 40 && Math.random()*100 < 3) {
					pauseTimer = 0;
					pausedAmplitude = true;
				}
				_body.velocity = Vec2.get(speed, Math.round(targetY - y));
				//var newy:Number = startingY + Math.sin((x / 100)) * (amplitude / 2);
				//trace (newy + '      ' + Math.sin(x/10)*(amplitude/2));
			}
			else {
				if (++pauseTimer > 20 && Math.random() * 100 < 5) {
					pausedAmplitude = false;
					pauseTimer = 0;
				}
				_body.velocity = Vec2.get(speed * 0.4, targetY - y);
				fire();
			}

			updateWeapons(timeDelta);
			//updateAnimation();
			
			if (x < -100) {
				kill = true;
			}
			
			if (hitPoints <= 0) {
				(_ce as uglycode.explosions.Explosions).registry.stats.ePenisLength += 30;
				(_ce as uglycode.explosions.Explosions).registry.stats.enemiesKilled['_small'] += 1;
				kill = true;
			}
			
			// update the animation
			var prevAnimation:String = _animation;
		}
		
		
		
		override public function destroy():void {
			//onJump.removeAll();
			//onGiveDamage.removeAll();
			Starling.juggler.remove(tween);
			onTakeDamage.removeAll();
			
			if (_beginContactListener) {
				_beginContactListener.space = null;
				_beginContactListener = null;
			}
			
			if (_endContactListener) {
				_endContactListener.space = null;
				_endContactListener = null;
			}
			
			super.destroy();
		}
		
		
		

		/**
		 * =================================================================== init ===========================================================================
		 */
		
		override protected function createShape():void {
			super.createShape();
			/*
			_body.shapes.add(new Polygon( [   Vec2.weak(6,34)   ,  Vec2.weak(28,29)   ,  Vec2.weak(40,16)   ,  Vec2.weak(6,10)   ] ));
			_body.shapes.add(new Polygon( [   Vec2.weak(28,29)   ,  Vec2.weak(44,29)   ,  Vec2.weak(44,20)   ,  Vec2.weak(40,16) ] ));
			//_body.localCOM = Vec2.get(0,42);
			//_body.scaleShapes(0.999, 1.001);
			_body.translateShapes(Vec2.weak(-30,-21));
			//_body.align(); //this will reset my translateShapes above
			*/
		}
		
		
		
		override protected function createConstraint():void {
			
			_body.space = _nape.space;
			_body.cbTypes.add(ENEMY);
			
			//I think this handles collision detection with ANY_BODY, interesting
			//_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, ENEMY, CbType.ANY_BODY.excluding(ENEMY).excluding(Shot.ENEMYSHOT), handleBeginContact);
			//_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, ENEMY, CbType.ANY_BODY.excluding(ENEMY).excluding(Shot.ENEMYSHOT), handleEndContact);
			
			_beginContactListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, ENEMY, Shot.SHOT.including(Ship.SHIP), handleBeginContact);
			//_endContactListener = new InteractionListener(CbEvent.END, InteractionType.COLLISION, ENEMY, Shot.SHOT.including(Ship.SHIP), handleEndContact);
			
			_body.space.listeners.add(_beginContactListener);
			//_body.space.listeners.add(_endContactListener);
			_body.gravMass = 0;
			
			//this is last function called in initialize(), so let's create an easily gettable velocity (please don't backfire on me)
			//wait, why did I need this in the first place?! lolz.
			_velocity = _body.velocity;
		}
		
		
		
		override protected function createBody():void {
			
			super.createBody();
			
			_body.allowRotation = false;
		}
		
		
		
		/**
		 * Don't collide with own shots, missiles and decorative scenery 
		 */
		override protected function createFilter():void {
			_body.setShapeFilters(new InteractionFilter(PhysicsCollisionCategories.ENEMY, ~(PhysicsCollisionCategories.ENEMY | PhysicsCollisionCategories.ENEMYSHOT | PhysicsCollisionCategories.ENEMYMISSILE | PhysicsCollisionCategories.SCENERY)) );
		}
		
		
		
		override public function initialize(poolObjectParams:Object = null):void {
			startingY = poolObjectParams.y;
			defaultHitPoints = poolObjectParams.hitPoints;
			kill = false;
			this.tween.reset(this, 4, Transitions.EASE_OUT_IN);
			tween.animate("targetY", (startingY > 230 ? 55 : 430));
			tween.repeatCount = 4;
			tween.reverse = true;
			//tween.onComplete = function():void { trace("tween complete!"); };
			Starling.juggler.add(tween);
			super.initialize(poolObjectParams);
		}
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		protected function updateWeapons(timeDelta:Number):void
		{
			for (var i:int = 0; i < weapons.length; i++)
			{
				// TODO add any engine/shield/power/rigs/EMP checks here inside Ship
				//weapons[i].fire();
				weapons[i].update(timeDelta);
			}
		}
		
		
		/**
		 * Override this method to handle the begin contact collision.
		 */
		override public function handleBeginContact(callback:InteractionCallback):void {
			//trace ('one: ' + getQualifiedClassName(callback.int1.userData.myData) + ', the other:' + getQualifiedClassName(callback.int2.userData.myData));
			if (callback.int1.userData.myData === this) {
				if (callback.int2.userData.myData is Shot || callback.int2.userData.myData is Missile) {
					//trace ('taking damage ' + callback.int2.userData.myData.damage);
					takeDamage(callback.int2.userData.myData.damage);
				}
				if (callback.int2.userData.myData is Ship) {
					this.takeDamage(7);
					callback.int2.userData.myData.takeDamage(7);
				}
			}
			
		}
		
		public function takeDamage(dmg:Number):void
		{
			// cycki
			// if shield...
			hitPoints -= dmg;
			checkHealth();
		}
		
		// TODO rework as damage setter? too magical? or Event onChange?
		protected function checkHealth():void
		{
			// TODO Auto Generated method stub
			if (hitPoints <= 0) {
				(_ce as Explosions).registry.stats.ePenisLength += score;
				this.playDeathSound();
				kill = true;
				hitPoints = defaultHitPoints;
			}
		}		
		
		protected function playDeathSound():void
		{
			_ce.sound.playSound(getDeathSound(), 1, 1);
		}
		
		protected function getDeathSound():String {
			trace('override getDeathSound() in Enemy subclass plzkthxbai');
			return '';
		}
		
		
		public function fire():void
		{
			for (var i:int = 0; i < weapons.length; i++)
			{
				// TODO add any engine/shield/power/rigs/EMP checks here inside Ship
				//weapons[i].fire();
				weapons[i].fire();
			}
		}
		
		

		
	}
}

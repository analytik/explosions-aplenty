package uglycode.util
{
	import flash.display.Stage;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.ui.Multitouch;

	public class Debug
	{
		public var stageInstance:Stage;
		
		public function Debug(stageInstance:Stage)
		{
		}

		public function TraceBasicInfo():void {
			trace ('stage width + height: ' + stageInstance.stageWidth + 'x' + stageInstance.stageHeight);
			trace ("Screen DPI: " + Capabilities.screenDPI);
			var serverString:String = unescape(Capabilities.serverString); 
			trace ('Other reported DPI: ' + Number(serverString.split("&DP=", 2)[1]));
			trace ("Resolution: " + Capabilities.screenResolutionX + 'x' + Capabilities.screenResolutionY);
			trace ("Touch Screen Type: " + Capabilities.touchscreenType);
			trace ("Mouse Cursor: " + Mouse.supportsCursor);
			trace ("Physical Keyboard Type: " + Keyboard.physicalKeyboardType);
			trace ("Virtual Keyboard: " + Keyboard.hasVirtualKeyboard);
			trace ("Gestures: " + Multitouch.supportsGestureEvents);
			trace ("Touch: " + Multitouch.supportsTouchEvents);
		}
	}
}
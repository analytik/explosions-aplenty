/**
 * Unused now
 */
package uglycode.util
{
	import com.uglycode.benchmark.Bullet;
	import com.uglycode.textures.SimpleMaterialGenerator;
	
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3D;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import flash.system.Capabilities;
	
	import alternativa.engine3d.controllers.SimpleObjectController;
	import alternativa.engine3d.core.Camera3D;
	import alternativa.engine3d.core.Object3D;
	import alternativa.engine3d.core.Resource;
	import alternativa.engine3d.core.View;
	import alternativa.engine3d.lights.AmbientLight;
	import alternativa.engine3d.lights.DirectionalLight;
	import alternativa.engine3d.loaders.TexturesLoader;
	import alternativa.engine3d.materials.FillMaterial;
	import alternativa.engine3d.primitives.Box;
	import alternativa.engine3d.resources.ExternalTextureResource;

	public class ServiceLocator
	{
		private var rootContainer:Object3D;
		private var stage:Stage;
		private var camera:Camera3D;
		private var controller:SimpleObjectController;
		private var stage3D:Stage3D;
		private var debugMode:Boolean = false;
		private var ambientLight:AmbientLight;
		private var directionalLight:DirectionalLight;

		public function ServiceLocator(stage1:Stage, debugMode:Boolean = false)
		{
			stage = stage1;
			rootContainer = new Object3D();
			stage.addEventListener(Event.RESIZE, onFirstResize);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			trace ('2stage width + height: ' + stage.stageWidth + 'x' + stage.stageHeight);
			stage.stageWidth = Capabilities.screenResolutionX;
			stage.stageHeight = Capabilities.screenResolutionY - 48;
			trace ('3stage width + height: ' + stage.stageWidth + 'x' + stage.stageHeight);
			stage.setAspectRatio(StageAspectRatio.LANDSCAPE);
		}

		
		private function onFirstResize(event:Event = null):void {
			trace ('onFirst Resize stage width + height: ' + stage.stageWidth + 'x' + stage.stageHeight);
			stage.removeEventListener(Event.RESIZE, onFirstResize);
			trace("[Hello World]", "in (pseudo)constructor");
			camera = new Camera3D(0.5, 8000);
			camera.x = 160;
			camera.y = -600;
			camera.z = 100;
			controller = new SimpleObjectController(stage, camera, 200);
			controller.lookAtXYZ(0,0,0);
			controller.accelerate(true);
			camera.view = new View(Capabilities.screenResolutionX, Capabilities.screenResolutionY, false, 0x306030, 0, 4);
			camera.view.hideLogo();
			stage.addChild(camera.view);
			camera.diagramAlign = StageAlign.BOTTOM_LEFT;
			stage.addChild(camera.diagram);
			
			
			rootContainer.addChild(camera);
			
			stage3D = stage.stage3Ds[0];
			stage3D.addEventListener(Event.CONTEXT3D_CREATE, deleteThisMethod);
			stage3D.requestContext3D();
		}

		private function deleteThisMethod(event:Event):void {
			// Upload resources
			var context3D:Context3D = stage3D.context3D;
			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(Event.RESIZE, onResize);
			onResize();
		}
		private function onEnterFrame(e:Event = null):void {
			controller.update();
			camera.render(stage3D);
		}		
		private function onResize(e:Event = null):void {
			trace ('onResize: stage width + height: ' + stage.stageWidth + 'x' + stage.stageHeight);
			camera.view.width = stage.stageWidth;
			camera.view.height = stage.stageHeight;
		}
		
		public function addBasicSetup():void {
			controller.lookAt(new Vector3D(0,0,0));
			addLights();
			//loadTextures();
		}

		
		private function addLights():void {
			ambientLight = new AmbientLight(0xd0d0d0);
			rootContainer.addChild(ambientLight);
			
			directionalLight = new DirectionalLight(0xFFFFFF);
			directionalLight.intensity = 1.5;
			directionalLight.x = 350;
			directionalLight.z = 500;
			directionalLight.y = -500;
			directionalLight.lookAt(0, 0, 0);
			rootContainer.addChild(directionalLight);
		}

	
		public function addBasicObjects():void {
			var box:Box = new Box(50,50,80);
			box.setMaterialToAllSurfaces(new FillMaterial(0xA00000));
			rootContainer.addChild(box);
			
			var box2:Box = new Box(100, 150, 150);
			box2.setMaterialToAllSurfaces(new FillMaterial(0x999999));
			box2.x = 130;
			box2.y = 0;
			rootContainer.addChild(box2);

			var invisibleObject:Box = new Box(1,1,1,1,1,1,false);
			rootContainer.addChild(invisibleObject);
		}
	
	}




}
package uglycode.util
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	public class Logger
	{
		public static var dayNamesShort:Array = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		public static var monthNamesShort:Array = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct","Nov", "Dec"];
		
		public function Logger()
		{
		}
		
		public static function log(text:String, logfile:String = 'stuff'):void {
			trace(text);
			var logFile:File = File.applicationStorageDirectory.resolvePath(logfile + '.log');
			var localFileStream:FileStream = new FileStream();
			localFileStream.open(logFile, FileMode.APPEND);
			var timestamp:String = toRFC822(new Date());
			localFileStream.writeMultiByte(timestamp + ' - ' + text + "\n", "utf-8");
			localFileStream.close();
		}
		
		/**
		 * Returns a date string formatted according to RFC822. Modified to not need MX framework
		 *
		 * @param d
		 *
		 * @returns
		 *
		 * @langversion ActionScript 3.0
		 * @playerversion Flash 9.0
		 * @tiptext
		 *
		 * @see http://asg.web.cmu.edu/rfc/rfc822.html
		 */	
		public static function toRFC822(d:Date):String
		{
			var date:Number = d.getUTCDate();
			var hours:Number = d.getUTCHours();
			var minutes:Number = d.getUTCMinutes();
			var seconds:Number = d.getUTCSeconds();
			var sb:String = new String();
			sb += Logger.dayNamesShort[d.getUTCDay()];
			sb += ", ";
			
			if (date < 10)
			{
				sb += "0";
			}
			sb += date;
			sb += " ";
			//sb += DateUtil.SHORT_MONTH[d.getUTCMonth()];
			sb += Logger.monthNamesShort[d.getUTCMonth()];
			sb += " ";
			sb += d.getUTCFullYear();
			sb += " ";
			if (hours < 10)
			{			
				sb += "0";
			}
			sb += hours;
			sb += ":";
			if (minutes < 10)
			{			
				sb += "0";
			}
			sb += minutes;
			sb += ":";
			if (seconds < 10)
			{			
				sb += "0";
			}
			sb += seconds;
			sb += " GMT";
			return sb;
		}
		

	}
}
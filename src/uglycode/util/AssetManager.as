package uglycode.util
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.Sound;
	import flash.net.URLRequest;
	//import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	//import starling.text.BitmapFont;
	//import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	import uglycode.util.Closure;
	//import com.tcg.util.Logger;
	
	public class AssetManager
	{
		//private static const logger:Logger = new Logger(Logger.INFO, "com.tcg.core.AssetManager");
		
		/** Location of grouped assets. */
		//public var assetsBase:String = "app:/assets/";
		public var assetsBase:String = "assets";
		
		// TODO: cache downloaded content
		//CONFIG::IOS
		//public var cacheBase:String = File.userDirectory.nativePath + "/Library/Private Documents/Offline/";
		
		//CONFIG::ANDROID
		public var cacheBase:String = "app-storage:/Offline/";
		
		private var _contentScaleFactor:int = 1;
		
		private var textures:Dictionary = new Dictionary();
		private var textureAtlases:Dictionary = new Dictionary();
		private var sounds:Dictionary = new Dictionary();
		
		private var textureAtlas:TextureAtlas;
		private var bitmapFontsLoaded:Boolean;
		public var baseEmbeddedAssets1x:Class;
		public var baseEmbeddedAssets2x:Class;
		
		
		
		public function AssetManager(x1:Class, x2:Class, stage:Stage)
		{
			this.baseEmbeddedAssets1x = x1;
			this.baseEmbeddedAssets2x = x2;
			
			// Calculate a temporary startup contentScaleFactor.
			// The true scale factor will be set in Scaffold after
			// the Starling object is created and initialized.
			
			// TODO kokot sprosty
			
			this._contentScaleFactor = stage.fullScreenWidth < 600 ? 2 : 1;
		}
		
		
		
		public function getBaseEmbeddedBitmap(name:String):Bitmap
		{
			// TODO FIX THIS SHEE
			// Should support more than just a simple 1 or 2
			return this.contentScaleFactor == 1
				? new this.baseEmbeddedAssets1x[name]()
				: new this.baseEmbeddedAssets2x[name]();
		}
		
		
		
		private function atlasKey(group:String, atlasName:String):String
		{
			return group + "|" + atlasName;
		}
		
		
		
		/**
		 * Load an image from an already-loaded atlas, given:
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param atlasName Name of the atlas file
		 * @param name Name of the image in the atlas file
		 *
		 * @return The loaded Image object, converted from the texture.
		 *
		 * @see loadAtlas
		 */
		public function getImageFromAtlas(group:String, atlasName:String, name:String, type:Class=null):Image
		{
			var t:Texture = getTextureFromAtlas(group, atlasName, name);
			return type ? new type(t) : new Image(t);
		}
		
		
		
		/**
		 * Load a movie clip from an already-loaded atlas, given:
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param atlasName Name of the atlas file
		 * @param name Name of the movie images in the atlas file
		 *
		 * @return The loaded MovieClip object, converted from the texture.
		 *
		 * @see loadAtlas
		 */
		public function getMovieFromAtlas(group:String, atlasName:String, name:String, type:Class=null):MovieClip
		{
			var t:Vector.<Texture> = getTexturesFromAtlas(group, atlasName, name);
			return type ? new type(t) : new MovieClip(t);
		}
		
		
		
		/**
		 * Load a texture from an already-loaded atlas, given:
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param atlasName Name of the atlas file
		 * @param name Name of the image in the atlas file
		 *
		 * @return The loaded Texture object.
		 *
		 * @see loadAtlas
		 */
		public function getTextureFromAtlas(group:String, atlasName:String, name:String):Texture
		{
			var atlas:TextureAtlas = textureAtlases[atlasKey(group, atlasName)];
			if (atlas == null)
			{
				return null;
			}
			
			var t:Texture = atlas.getTexture(name);
			//if (logger.isDebug()) logger.debug("texture: " + t.width + "x" + t.height + " scale: " + t.scale);
			
			return t;
		}
		
		
		
		/**
		 * Load a list of textures from an already-loaded atlas, given:
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param atlasName Name of the atlas file
		 * @param name Prefix of the images in the atlas file
		 *
		 * @return A vector of the textures in alphabetical order.
		 *
		 * @see loadAtlas
		 */
		public function getTexturesFromAtlas(group:String, atlasName:String, name:String):Vector.<Texture>
		{
			var atlas:TextureAtlas = textureAtlases[atlasKey(group, atlasName)];
			if (atlas == null)
			{
				return null;
			}
			
			return atlas.getTextures(name);
		}
		
		
		
		private function getGroupName(group:String, name:String):String
		{
			return File.applicationDirectory.url + this.assetsBase + "/" + group + "/" + name;
		}
		
		
		
		private function getGroupAtlasName(group:String, atlasName:String):String
		{
			return File.applicationDirectory.url + this.assetsBase + "/" + group + "/atlas-" + atlasName;
			//return getGroupName(group, atlasName + "_atlas");
		}
		
		/**
		 * Asynchronously load an image atlas. The necessity of an asynchronous load is the main
		 * difference between embedded assets and file assets.
		 *
		 * The callback signature for a successful load is:
		 * callback(atlas:TextureAtlas, group:String, atlasName:String)
		 *
		 * For an unsuccessful load:
		 * callback(null, group:String, atlasName:String)
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param atlasName Name of the atlas file
		 * @param callback Function to call when the load is complete.
		 *
		 * @see getImageFromAtlas
		 */
		public function loadAtlas(group:String, atlasName:String, callback:Function):void
		{
			var groupAtlasName:String = getGroupAtlasName(group, atlasName);
			//Logger.log('loading atlas: ' + groupAtlasName);
			var stream:FileStream = new FileStream();
			//trace(File.applicationDirectory.resolvePath('assets/temp/temp_atlas.xml').url);
			var file:File = new File(groupAtlasName + ".xml");
			stream.open(file, FileMode.READ);
			var xml:XML = XML(stream.readUTFBytes(stream.bytesAvailable));
			stream.close();
			
			var url:URLRequest = new URLRequest(groupAtlasName + ".png");
			var loader:Loader = new Loader();
			loader.load(url);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,
				Closure.create(this, this.onAtlasImageLoad,
					group, atlasName, xml,
					callback, loader));
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,
				Closure.create(this, this.onAtlasImageLoadError, group, atlasName, callback));
		}
		
		
		
		private function onAtlasImageLoad(e:Event, group:String, atlasName:String, xml:XML, callback:Function, loader:Loader):void
		{
			var bm:Bitmap = loader.content as Bitmap;
			// bitmap, generateMipMaps, optimizeForRenderTexture, scale
			var texture:Texture = Texture.fromBitmap(bm, false, false, this.contentScaleFactor);
			
			/* this is for... apple then?
			if (!CONFIG::ANDROID)
			{
				bm.bitmapData.dispose();
			}
			*/
			
			var atlas:TextureAtlas = new TextureAtlas(texture, xml);
			this.textureAtlases[atlasKey(group, atlasName)] = atlas;
			
			callback(atlas, group, atlasName);
		}
		
		
		
		private function onAtlasImageLoadError(e:Event, group:String, atlasName:String, callback:Function):void
		{
			// TODO pass the error event too.
			Logger.log("Error loading atlas " + group + '/' + atlasName + ' on eventType ' + e.type);
			callback(null, group, atlasName);
		}
		
		
		
		/**
		 * Load a text asset from an asset group.
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param name Name of the text file
		 *
		 * @return The text of the file as a String.
		 */
		public function loadText(group:String, name:String):String
		{
			var stream:FileStream = new FileStream();
			var file:File = new File(getGroupName(group, name));
			stream.open(file, FileMode.READ);
			var result:String = stream.readUTFBytes(stream.bytesAvailable);
			stream.close();
			
			return result;
		}
		
		
		
		/**
		 * Load a text asset from an asset group and parse it as JSON.
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param name Name of the text file, without the .json extension.
		 *
		 * @return An Object parsed from the text of the file.
		 */
		public function loadJson(group:String, name:String):Object
		{
			return JSON.parse(loadText(group, name + ".json"));
		}
		
		
		
		/**
		 * Load a text asset from an asset group and parse it as XML.
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param name Name of the text file.
		 *
		 * @return An Object parsed from the text of the file.
		 */
		public function loadXml(group:String, name:String):XML
		{
			return new XML(loadText(group, name));
		}
		
		
		
		/**
		 * Load a MP3 sound file from an asset group and prepare it for streaming playback.
		 *
		 * @param group Asset group (subdirectory under media)
		 * @param name Name of the text file
		 *
		 * @return An Object parsed from the text of the file.
		 */
		public function getSoundStreaming(group:String, name:String):Sound
		{
			var s:Sound = new Sound();
			var url:URLRequest = new URLRequest(getGroupName(group, name + ".mp3"));
			//var ctx:SoundLoaderContext = new SoundLoaderContext(1000, true)
			s.load(url);
			return s;
		}
		
		
		
		/* TODO
		public function loadBitmapFonts():void
		{
		if (!this.bitmapFontsLoaded)
		{
		var texture:Texture = getTexture("DesyrelTexture");
		var xml:XML = XML(create("DesyrelXml"));
		TextField.registerBitmapFont(new BitmapFont(texture, xml));
		this.bitmapFontsLoaded = true;
		}
		}
		*/
		
		
		
		public function get contentScaleFactor():Number { return this._contentScaleFactor; }
		public function set contentScaleFactor(value:Number):void
		{
			//if (logger.isInfo()) logger.info("Setting contentScaleFactor: " + value);
			
			for each (var texture:Texture in this.textures)
			{
				texture.dispose();
			}
			
			// TODO FIX THIS SHEE
			this.textures = new Dictionary();
			this._contentScaleFactor = value < 1.5 ? 1 : 2; // assets are available for factor 1 and 2 
			
			//if (logger.isInfo()) logger.info("Normalized contentScaleFactor: " + this._contentScaleFactor);
		}
	}
}
package {

    import Box2DAS.Collision.Shapes.b2PolygonShape;
	import Box2DAS.Common.V2;

	import com.citrusengine.objects.PhysicsObject;

	/**
	 * @author Aymeric
	 * <p>This is a class created by the software http://www.physicseditor.de/</p>
	 * <p>Just select the CitrusEngine template, upload your png picture, set polygons and export.</p>
	 * <p>Be careful, the registration point is topLeft !</p>
	 * @param peObject : the name of the png file
	 */
    public class PhysicsEditorObjects extends PhysicsObject {
		
		[Inspectable(defaultValue="")]
		public var peObject:String = "";

		private var _tab:Array;

		public function PhysicsEditorObjects(name:String, params:Object = null) {

			super(name, params);
		}

		override public function destroy():void {

			super.destroy();
		}

		override public function update(timeDelta:Number):void {

			super.update(timeDelta);
		}

		override protected function defineFixture():void {
			
			super.defineFixture();
			
			_createVertices();

			_fixtureDef.density = _getDensity();
			_fixtureDef.friction = _getFriction();
			_fixtureDef.restitution = _getRestitution();
			
			for (var i:uint = 0; i < _tab.length; ++i) {
				var polygonShape:b2PolygonShape = new b2PolygonShape();
				polygonShape.Set(_tab[i]);
				_fixtureDef.shape = polygonShape;

				body.CreateFixture(_fixtureDef);
			}
		}
		
        protected function _createVertices():void {
			
			_tab = [];
			var vertices:Vector.<V2> = new Vector.<V2>();

			switch (peObject) {
				
				case "bolt_a_01":
											
			        vertices.push(new V2(10/_box2D.scale, 11/_box2D.scale));
					vertices.push(new V2(0/_box2D.scale, 6/_box2D.scale));
					vertices.push(new V2(10/_box2D.scale, 2/_box2D.scale));
					
					_tab.push(vertices);
					
					break;
			
				case "glob_a_01":
					
					break;
			
				case "gun_a_01":
											
			        vertices.push(new V2(7/_box2D.scale, 4/_box2D.scale));
					vertices.push(new V2(0/_box2D.scale, 4/_box2D.scale));
					vertices.push(new V2(0/_box2D.scale, 2/_box2D.scale));
					vertices.push(new V2(7/_box2D.scale, 2/_box2D.scale));
					
					_tab.push(vertices);
					
					break;
			
				case "ship":
											
			        vertices.push(new V2(6/_box2D.scale, 10/_box2D.scale));
					vertices.push(new V2(40/_box2D.scale, 16/_box2D.scale));
					vertices.push(new V2(28/_box2D.scale, 29/_box2D.scale));
					vertices.push(new V2(6/_box2D.scale, 34/_box2D.scale));
					
					_tab.push(vertices);
					vertices = new Vector.<V2>();
											
			        vertices.push(new V2(40/_box2D.scale, 16/_box2D.scale));
					vertices.push(new V2(44/_box2D.scale, 20/_box2D.scale));
					vertices.push(new V2(44/_box2D.scale, 29/_box2D.scale));
					vertices.push(new V2(28/_box2D.scale, 29/_box2D.scale));
					
					_tab.push(vertices);
					
					break;
			
				case "ufo1":
											
			        vertices.push(new V2(7/_box2D.scale, 37/_box2D.scale));
					vertices.push(new V2(5/_box2D.scale, 20/_box2D.scale));
					vertices.push(new V2(33/_box2D.scale, 4/_box2D.scale));
					vertices.push(new V2(59/_box2D.scale, 37/_box2D.scale));
					
					_tab.push(vertices);
					
					break;
			
				case "ufo2":
											
			        vertices.push(new V2(48/_box2D.scale, 33/_box2D.scale));
					vertices.push(new V2(16/_box2D.scale, 33/_box2D.scale));
					vertices.push(new V2(7/_box2D.scale, 27/_box2D.scale));
					vertices.push(new V2(57/_box2D.scale, 7/_box2D.scale));
					
					_tab.push(vertices);
					
					break;
			
			}
		}

		protected function _getDensity():Number {

			switch (peObject) {
				
				case "bolt_a_01":
					return 1;return 1;
					break;
			
				case "glob_a_01":
					return 1;
					break;
			
				case "gun_a_01":
					return 1;
					break;
			
				case "ship":
					return 1;
					break;
			
				case "ufo1":
					return 1;
					break;
			
				case "ufo2":
					return 1;
					break;
			
			}

			return 1;
		}
		
		protected function _getFriction():Number {
			
			switch (peObject) {
				
				case "bolt_a_01":
					return 0.6;return 0.6;
					break;
			
				case "glob_a_01":
					return 0.6;
					break;
			
				case "gun_a_01":
					return 0.6;
					break;
			
				case "ship":
					return 0.6;
					break;
			
				case "ufo1":
					return 0.6;
					break;
			
				case "ufo2":
					return 0.6;
					break;
			
			}

			return 0.6;
		}
		
		protected function _getRestitution():Number {
			
			switch (peObject) {
				
				case "bolt_a_01":
					return 0.3;return 0.3;
					break;
			
				case "glob_a_01":
					return 0.3;
					break;
			
				case "gun_a_01":
					return 0.3;
					break;
			
				case "ship":
					return 0.3;
					break;
			
				case "ufo1":
					return 0.3;
					break;
			
				case "ufo2":
					return 0.3;
					break;
			
			}

			return 0.3;
		}
	}
}
